package edu.neu.madcourse.shresthbhatnagar.hellomad;

import android.content.Intent;
import android.util.Log;

import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.WearableListenerService;

/**
 * Created by vidhu on 4/12/2017.
 */

public class MessageReceiverService extends WearableListenerService {
    public static final String TAG = MessageReceiverService.class.getSimpleName();


    @Override
    public void onDataChanged(DataEventBuffer dataEventBuffer) {
        super.onDataChanged(dataEventBuffer);
        Log.d(TAG, "Message Changed");
    }

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        super.onMessageReceived(messageEvent);
        if(messageEvent.getPath().contains("start")) {
            Log.d(TAG, "Starting Measurement");
            startService(new Intent(this, SensorService.class));
        }else if(messageEvent.getPath().contains("stop")) {
            Log.d(TAG, "Stopping Measurement");
            stopService(new Intent(this, SensorService.class));
        }
    }
}
