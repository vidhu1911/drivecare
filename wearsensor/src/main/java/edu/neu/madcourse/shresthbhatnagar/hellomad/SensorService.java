package edu.neu.madcourse.shresthbhatnagar.hellomad;

import android.app.Notification;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;

public class SensorService extends Service implements GoogleApiClient.ConnectionCallbacks, SensorEventListener{
    public static final String TAG = SensorService.class.getSimpleName();

    //Data
    GoogleApiClient googleApiClient;
    SensorManager sensorManager;
    Sensor sensor;

    public SensorService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Bitmap icon = BitmapFactory.decodeResource(this.getResources(), R.drawable.notification_bg);

        Notification.Builder builder = new Notification.Builder(this);
        builder.setContentTitle("DriveCare");
        builder.setContentText("Collecting sensor data..");
        builder.setSmallIcon(R.drawable.car_map_marker)
        .extend(new Notification.WearableExtender().setBackground(icon));

        startForeground(1, builder.build());

        this.googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addApi(Wearable.API)
                .build();

        this.sensorManager = (SensorManager) this.getSystemService(Context.SENSOR_SERVICE);
        this.sensor = this.sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        this.sensorManager.registerListener(this, this.sensor, SensorManager.SENSOR_DELAY_NORMAL);
        this.googleApiClient.connect();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.sensorManager.unregisterListener(this);
        this.googleApiClient.disconnect();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.d(TAG, "onConnected: " + bundle);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        this.sendSensorData(event.timestamp, event.values);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }


    public void sendSensorData(long timestamp, float[] values){
        PutDataMapRequest putDataMapReq = PutDataMapRequest.create("/acc");
        putDataMapReq.getDataMap().putLong("time", timestamp);
        putDataMapReq.getDataMap().putFloatArray("values", values);

        PutDataRequest putDataReq = putDataMapReq.asPutDataRequest();
        send(putDataReq);
    }

    private void send(PutDataRequest putDataRequest) {
        if (this.googleApiClient.isConnected()) {
            Wearable.DataApi.putDataItem(googleApiClient, putDataRequest).setResultCallback(new ResultCallback<DataApi.DataItemResult>() {
                @Override
                public void onResult(DataApi.DataItemResult dataItemResult) {
                    Log.v(TAG, "Sending sensor data: " + dataItemResult.getStatus().isSuccess());
                }
            });
        }
    }
}
