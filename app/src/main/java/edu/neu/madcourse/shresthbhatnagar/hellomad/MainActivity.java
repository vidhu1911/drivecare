package edu.neu.madcourse.shresthbhatnagar.hellomad;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import edu.neu.madcourse.shresthbhatnagar.drivecare.activities.FinalProject;
import edu.neu.madcourse.shresthbhatnagar.scroggle.MainMenu;
import edu.neu.madcourse.shresthbhatnagar.com.MenuActivity;
import edu.neu.madcourse.shresthbhatnagar.tictactoe.TicTacToe;
import edu.neu.madcourse.shresthbhatnagar.dictionary.TestDictionary;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.setTitle(R.string.my_name);
    }

    public void openAboutActivity(View view) {
        Intent intent = new Intent(this, About.class);
        this.startActivity(intent);
    }

    public void runtictactoe(View view) {
        Intent intent = new Intent(this, TicTacToe.class);
        this.startActivity(intent);
    }

    public void runDictionary(View view) {
        Intent intent = new Intent(this, TestDictionary.class);
        this.startActivity(intent);
    }

    public void generateError(View view) {
        View errorView = null;
        errorView.animate();
    }

    public void runScroggle(View view) {
        Intent intent = new Intent(this, MainMenu.class);
        this.startActivity(intent);
    }

    public void runCom(View view) {
        Intent intent = new Intent(this, MenuActivity.class);
        this.startActivity(intent);
    }

    public void runScroggle2(View view) {
        Intent intent = new Intent(this, edu.neu.madcourse.shresthbhatnagar.scroggle2.PlayerList.class);
        this.startActivity(intent);
    }

    public void runTrkp(View view) {
        Intent intent = new Intent(this, edu.neu.madcourse.shresthbhatnagar.drivecare.activities.TrickyPart.class);
        this.startActivity(intent);
    }

    public void runFinal(View view) {
        Intent intent = new Intent(this, FinalProject.class);
        this.startActivity(intent);
    }

    public void exit(View view) {
        this.finish();
        System.exit(0);
    }
}
