package edu.neu.madcourse.shresthbhatnagar.dictionary;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author vidhu
 */
public class Trie {

    TrieNode root = new TrieNode(null);

    public void insert(String s) {
        TrieNode curr = root;

        for(int i=0, n=s.length(); i < n; i++){
            Character c = s.charAt(i);

            if(!curr.childExist(c))
                curr.addChild(c);

            curr = curr.getChild(c);
        }
    }

    public boolean search(String s) {

        TrieNode curr = root;
        for(int i=0, n=s.length(); i < n; i++){
            Character c = s.charAt(i);

            if(!curr.childExist(c))
                return false;

            curr = curr.getChild(c);
        }

        return true;
    }

    class TrieNode {
        Character value;
        Map<Character, TrieNode> children = new HashMap<Character, TrieNode>();

        public TrieNode(Character value){
            this.value = value;
        }

        public TrieNode getChild(Character c){
            return this.children.get(c);
        }

        public void addChild(Character c){
            children.put(c, new TrieNode(c));
        }

        public boolean childExist(Character c){
            return this.children.get(c) != null;
        }

        @Override
        public String toString(){
            return value.toString();
        }
    }
}
