package edu.neu.madcourse.shresthbhatnagar.scroggle;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.AsyncTask;
import android.os.Vibrator;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import edu.neu.madcourse.shresthbhatnagar.hellomad.R;

public class GameActivity extends AppCompatActivity {
    public static final long TIMER_VALUE = 120000;
    public static final String WORD_LIST = "word_list";
    public static final String KEY_RESTORE = "key_restore";
    public static final String PHASE_RESTORE = "phase_restore";
    public static final String PREF_RESTORE1 = "pref_restore_phase1";
    public static final String TIMER_RESTORE = "timer_restore";

    Vibrator v;
    MediaPlayer mMediaPlayer;
    SoundPool soundPool;
    int clickSound;
    int successSound;

    AsyncTask<BufferedReader, String, Void> wordLoader;
    HashMap<String, Boolean> words = new HashMap<>();

    CountDownTimer timer;

    View pauseOverlay;
    Button btnUnpauseGame;
    GameFragment gameFragment;
    Game2Fragment game2Fragment;
    List<String> wordsFound = new ArrayList<>();
    int currentPhase = 1;
    TextView tvcurrword;
    TextView tvScore;
    TextView tvTimer;
    long msLeft;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scroggle_game);

        this.v = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
        this.soundPool = new SoundPool(10, AudioManager.STREAM_MUSIC, 0);
        this.clickSound = this.soundPool.load(this, R.raw.button_29, 1);
        this.successSound = this.soundPool.load(this, R.raw.success, 1);

        this.gameFragment = new GameFragment();
        this.game2Fragment = new Game2Fragment();

        this.tvcurrword = (TextView) findViewById(R.id.tv_currWord);
        this.tvScore = (TextView) findViewById(R.id.tv_score);
        this.tvTimer = (TextView) findViewById(R.id.tv_timer);

        this.pauseOverlay = findViewById(R.id.pause_overlay);
        this.btnUnpauseGame = (Button) findViewById(R.id.btn_unpauseGame);
        this.btnUnpauseGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resumeGame();
            }
        });

        boolean restore = getIntent().getBooleanExtra(KEY_RESTORE, false);
        if(!restore){
            FragmentTransaction fTransaction = getSupportFragmentManager().beginTransaction();
            fTransaction.replace(R.id.fragment_scroggle_game, this.gameFragment);
            fTransaction.commit();
            startTimer();
            this.setTitle("Phase 1");
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        boolean restore = getIntent().getBooleanExtra(KEY_RESTORE, false);
        if (restore)
            restoreGame();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.scroggle_game, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Drawable drawable;
        switch (item.getItemId()) {
            case R.id.menu_scrMute:
                this.toggleMuteBackgroundMusic();
                if (this.mMediaPlayer.isPlaying()) {
                    drawable = ContextCompat.getDrawable(getApplicationContext(), R.drawable.speaker_mute);
                    item.setIcon(drawable);
                } else {
                    drawable = ContextCompat.getDrawable(getApplicationContext(), R.drawable.speaker_unmute);
                    item.setIcon(drawable);
                }
                break;
            case R.id.menu_scrViewWords:
                this.viewWordList();
                break;
        }
        return true;
    }

    @Override
    protected void onPause() {
        super.onPause();

        //Stop background music
        mMediaPlayer.stop();
        mMediaPlayer.reset();
        mMediaPlayer.release();

        String gameData = "";
        if (currentPhase == 1) {
            gameData = this.gameFragment.getState();
        } else {
            gameData = this.game2Fragment.getState();
        }

        //Save Phase
        this.getPreferences(MODE_PRIVATE).edit()
                .putInt(PHASE_RESTORE, this.currentPhase)
                .commit();
        //Save Game Data
        this.getPreferences(MODE_PRIVATE).edit()
                .putString(PREF_RESTORE1, gameData)
                .commit();
        //Save Timer Value
        this.getPreferences(MODE_PRIVATE).edit()
                .putLong(TIMER_RESTORE, this.msLeft)
                .commit();
        //Save Words Found
        String wordsFoundSerialized = "";
        for(String word : this.wordsFound){
            wordsFoundSerialized += word + ",";
        }
        this.getPreferences(MODE_PRIVATE).edit()
                .putString(WORD_LIST, wordsFoundSerialized)
            .commit();

        if (this.timer != null)
            this.timer.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();

        //Start background music
        this.mMediaPlayer = MediaPlayer.create(this, R.raw.bensound_sweet);
        this.mMediaPlayer.setLooping(true);
        this.mMediaPlayer.start();

        if (this.timer != null && this.timer.isPaused())
            this.timer.resume();
    }

    protected void restoreGame() {
        this.currentPhase = getPreferences(MODE_PRIVATE).getInt(PHASE_RESTORE, 1);

        //Restore words
        String[] wordsFoundUnserialized = getPreferences(MODE_PRIVATE).getString(WORD_LIST, "").split(",");
        this.wordsFound = new ArrayList<>(Arrays.asList(wordsFoundUnserialized));

        if (this.currentPhase == 1) {
            //Set view to phase 1
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fTransaction = fragmentManager.beginTransaction();
            fTransaction.replace(R.id.fragment_scroggle_game, this.gameFragment);
            fTransaction.commit();
            fragmentManager.executePendingTransactions();

            //Get Saved game data for phase 1
            String gameData = getPreferences(MODE_PRIVATE).getString(PREF_RESTORE1, null);
            if (gameData != null) {
                this.gameFragment.putState(gameData);
                this.setScore(this.gameFragment.getScore());
                this.updateCurrentWord(this.gameFragment.getCurrentWord());

                this.msLeft = getPreferences(MODE_PRIVATE).getLong(TIMER_RESTORE, TIMER_VALUE);
                this.startTimer(this.msLeft);
            } else {
                startTimer();
            }
            this.setTitle("Phase 1");
        } else {
            //Set view to phase 2
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fTransaction = fragmentManager.beginTransaction();
            fTransaction.replace(R.id.fragment_scroggle_game, this.game2Fragment);
            fTransaction.commit();
            fragmentManager.executePendingTransactions();

            //Get Saved game data for phase 2
            String gameData = getPreferences(MODE_PRIVATE).getString(PREF_RESTORE1, null);
            if (gameData != null) {
                this.game2Fragment.putState(gameData);
                this.setScore(this.game2Fragment.getScore());
                this.updateCurrentWord(this.game2Fragment.getCurrentWord());

                this.msLeft = getPreferences(MODE_PRIVATE).getLong(TIMER_RESTORE, TIMER_VALUE);
                this.startTimer(this.msLeft);
            } else {
                startTimer();
            }
            this.setTitle("Phase 2");
        }
    }

    protected void startTimer() {
        this.startTimer(TIMER_VALUE);
    }

    protected void startTimer(long time) {
        this.timer = new CountDownTimer(time, 1000) {
            boolean warningShown = false;
            @Override
            public void onTick(long millisUntilFinished) {
                msLeft = millisUntilFinished;
                tvTimer.setText("Time: " + millisUntilFinished / 1000);
                if(!warningShown && millisUntilFinished < 10000) {
                    Toast.makeText(getBaseContext(), "Time is almost up!", Toast.LENGTH_LONG).show();
                    this.warningShown = true;
                }
            }

            @Override
            public void onFinish() {
                msLeft = 0;
                tvTimer.setText("Time: 0");
                Toast t = Toast.makeText(getBaseContext(), "Times up!", Toast.LENGTH_SHORT);
                t.show();
                if (currentPhase == 1)
                    phaseOneEnd();
                else
                    phaseTwoEnd();
            }
        };
        this.timer.start();
    }

    protected void unselectAll() {
        if (this.currentPhase == 1)
            this.gameFragment.unselectAll();
        else
            this.game2Fragment.unselectAll();
    }

    protected void submitWord() {
        String word = this.tvcurrword.getText().toString();

        if (word.length() < 3)
            return;

        if (this.words.containsKey(word)) {
            //Add word to found list
            this.wordsFound.add(word);

            //Play sound effect
            this.soundPool.play(this.successSound, 1, 1, 1, 0, 1);

            if (this.currentPhase == 1) {
                this.gameFragment.finishSmall();
                this.setScore(this.gameFragment.getScore() + (word.length() * 10));
            } else {
                this.game2Fragment.finishSmall();
                this.setScore(this.game2Fragment.getScore() + (word.length() * 20));
            }
        } else {
            if (this.currentPhase == 1) {
                this.setScore(this.gameFragment.getScore() - 50);
            } else {
                this.setScore(this.game2Fragment.getScore() - 50);
            }
            unselectAll();
        }
    }

    protected void setScore(int score) {
        if (this.currentPhase == 1)
            this.gameFragment.setScore(score);
        else
            this.game2Fragment.setScore(score);
        this.tvScore.setText("Score: " + score);
    }

    protected void updateCurrentWord(String word) {
        this.v.vibrate(25);
        this.soundPool.play(clickSound, 1.0f, 1.0f, 0, 0, 1);
        if (word.length() == 1)
            loadWords(word.substring(0, 1));

        this.tvcurrword.setText(word);
    }

    protected void loadWords(String first) {
        AssetManager assetManager = this.getAssets();

        InputStream is = null;
        try {
            is = assetManager.open("dict_words/dict." + first + ".txt");
        } catch (Exception ex) {
            ex.printStackTrace();
            return;
        }
        BufferedReader br = new BufferedReader(new InputStreamReader(is));


        this.wordLoader = new WordLoader();
        this.wordLoader.execute(br);
    }

    protected void phaseOneEnd() {
        new AlertDialog.Builder(this)
                .setTitle("Onto next phase!")
                .setMessage("Time is up for phase one!\n" +
                        "Get ready for phase two")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        startPhaseTwo();
                    }
                })
                .setIcon(android.R.drawable.ic_lock_idle_alarm)
                .show();
        this.updateCurrentWord("");
    }

    protected void startPhaseTwo() {
        String[] letters = gameFragment.getRemainingLetters();

        game2Fragment.seedLetters(letters);
        game2Fragment.setScore(gameFragment.getScore());
        FragmentTransaction fTransaction = getSupportFragmentManager().beginTransaction();
        fTransaction.replace(R.id.fragment_scroggle_game, game2Fragment);
        fTransaction.commit();

        this.currentPhase = 2;
        this.setTitle("Phase 2");
        startTimer();
    }

    protected void phaseTwoEnd() {
        new AlertDialog.Builder(this)
                .setTitle("Congratulations!")
                .setMessage("Time is up! Your final score is\n " + this.tvScore.getText())
                .setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        finish();
                    }
                })
                .setIcon(android.R.drawable.ic_lock_idle_alarm)
                .show();
    }

    public MediaPlayer toggleMuteBackgroundMusic() {
        if (this.mMediaPlayer.isPlaying()) {
            this.mMediaPlayer.pause();
        } else
            this.mMediaPlayer.start();
        return this.mMediaPlayer;
    }

    public void viewWordList(){
        Intent intent = new Intent(this, ViewWordsActivity.class);
        intent.putExtra(WORD_LIST, this.wordsFound.toArray(new String[0]));
        startActivity(intent);
    }

    public void pauseGame() {
        this.timer.pause();
        this.pauseOverlay.setVisibility(View.VISIBLE);
    }

    public void resumeGame() {
        this.timer.resume();
        this.pauseOverlay.setVisibility(View.INVISIBLE);
    }

    class WordLoader extends AsyncTask<BufferedReader, String, Void> {

        @Override
        protected Void doInBackground(BufferedReader... reader) {
            try {
                String line;
                while ((line = reader[0].readLine()) != null) {
                    words.put(line, true);
                }
            } catch (Exception ex) {
                Log.d("Error:", ex.getMessage());
            }

            return null;
        }

    }

}
