package edu.neu.madcourse.shresthbhatnagar.scroggle;

import android.view.View;

import java.util.Random;

/**
 * Created by vidhu on 2/13/2017.
 */

public class TileGame {
    enum Type {
        FULL, PART
    }

    private static Random r = new Random();
    private static int[][] pathIdxArr = {
            {0, 3, 4, 1, 2, 5, 8, 7, 6},
            {1, 2, 5, 4, 0, 3, 6, 7, 8},
            {2, 5, 1, 4, 0, 3, 6, 7, 8},
            {3, 6, 7, 8, 5, 4, 0, 1, 2},
            {4, 1, 2, 5, 8, 7, 6, 3, 0},
            {5, 8, 7, 6, 3, 4, 0, 1, 2},
            {6, 7, 4, 3, 0, 1, 2, 5, 8},
            {7, 6, 3, 0, 4, 8, 5, 2, 1},
            {8, 5, 2, 1, 4, 7, 6, 3, 0},
    };

    private final GameFragment mGame;
    private int pathIdx;
    private final Type type;
    private String word;
    private TileLetter mSubTiles[];
    private View mView;
    private boolean isPlayable = true;

    public TileGame(GameFragment fragment, Type type) {
        this.mGame = fragment;
        this.type = type;
        this.pathIdx = r.nextInt(9);
    }

    public int getPathIdx() {
        return pathIdx;
    }

    public void setPathIdx(int pathIdx) {
        this.pathIdx = pathIdx;
    }

    public String getWord() {
        return this.word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public void setSubTiles(TileLetter[] tiles) {
        this.mSubTiles = tiles;
    }

    public void setView(View rootView) {
        this.mView = rootView;
    }

    public boolean isPlayable() {
        return isPlayable;
    }

    public void setPlayable(boolean playable) {
        isPlayable = playable;
    }

    public void initTiles(){
        if(this.type == Type.PART) {
            int[] path = pathIdxArr[pathIdx];
            for (int i = 0; i < 9; i++) {
                TileLetter tile = this.mSubTiles[path[i]];
                String letter = Character.toString(this.word.charAt(i));
                tile.setLetter(letter);
            }
        }
    }

    public void updateDrawableState() {

    }

}
