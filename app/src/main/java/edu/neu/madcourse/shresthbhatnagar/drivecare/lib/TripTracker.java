package edu.neu.madcourse.shresthbhatnagar.drivecare.lib;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.JsonArray;

import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by vidhu on 4/11/2017.
 */

public class TripTracker {
    private static final String TAG = TripPoint.class.getSimpleName();

    private Context context;
    private Location lastKnownLocation;
    private Map<Long, TripPoint> tripPoints = new HashMap<>();
    private List<Vector3D> wearAcc = new ArrayList<>();
    private StaticMapGenerator mapGenerator;

    public TripTracker(Context context) {
        this.context = context;
        mapGenerator = new StaticMapGenerator(context);
    }

    public void addPoint(Location location) {
        this.lastKnownLocation = location;

        TripPoint tripPoint = new TripPoint(location.getLatitude(), location.getLongitude());
        this.addTripPoint(tripPoint);

        this.mapGenerator.addLocation(location);
    }

    public TripPoint addPoint(int event) {
        if (lastKnownLocation == null) return null;
        TripPoint tripPoint = new TripPoint(lastKnownLocation.getLatitude(), lastKnownLocation.getLongitude(), event);
        this.addTripPoint(tripPoint);
        return tripPoint;
    }

    public void addWearPoint(Vector3D vector3D) {
        this.wearAcc.add(vector3D);
    }

    public void addTripPoint(TripPoint tripPoint) {
        this.tripPoints.put(tripPoint.getTime(), tripPoint);
    }

    public void dismissTripPoint(long time){
        this.tripPoints.remove(time);
    }

    public void saveToSDCard() {
        Gson gson = new Gson();

        Map<String, List> data = new HashMap<>();
        List<TripPoint> tripPoints = new ArrayList<>(this.tripPoints.values());
        Collections.sort(tripPoints);
        data.put("trip", tripPoints);
        data.put("wear", this.wearAcc);


        String json = gson.toJson(data);
        String filename = this.getFileName();

        File path = new File(Environment.getExternalStorageDirectory() + "/drivecare");
        File file = new File(path, filename + ".json");
        try {
            path.mkdirs();
            file.createNewFile();
            OutputStream os = new FileOutputStream(file);
            os.write(json.getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.mapGenerator.generateImage(new File(path, filename + ".png"));
    }


    private String getFileName() {
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
        return "DriveCare." + dateFormat.format(date);
    }

    public boolean checkPermission(Context context) {
        return ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    public void askPermission(Activity activity) {
        if (checkPermission(activity)) return;
        ActivityCompat.requestPermissions(activity,
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
    }

    public class TripPoint implements Comparable<TripPoint> {
        private long time;
        private double lat;
        private double lng;
        private int event;


        public TripPoint(double lat, double lng, int event) {
            this.time = System.currentTimeMillis();
            this.lat = lat;
            this.lng = lng;
            this.event = event;
        }

        public TripPoint(double lat, double lng) {
            this(lat, lng, -1);
        }

        public LatLng getLocation() {
            return new LatLng(this.lat, this.lng);
        }

        public boolean isEvent(){
            return this.event != -1;
        }

        public long getTime() {
            return time;
        }

        public double getLat() {
            return lat;
        }

        public double getLng() {
            return lng;
        }

        public int getEvent() {
            return event;
        }

        public Date getTimeasDate(){
            return new Date(this.time);
        }

        @Override
        public int compareTo(@NonNull TripPoint o) {
            if(this.time < o.getTime()){
                return 1;
            }else if(this.time == o.getTime()){
                return 0;
            }else{
                return -1;
            }
        }
    }

    public static class Vector3D {
        public float x;
        public float y;
        public float z;

        public Vector3D(float x, float y, float z) {
            this.x = x;
            this.y = y;
            this.z = z;
        }
    }

    public static void deleteAll(){
        File path = new File(Environment.getExternalStorageDirectory() + "/drivecare");
        try {
            FileUtils.deleteDirectory(path);
        } catch (IOException e) {
            Log.d(TAG, Log.getStackTraceString(e));
        }
    }
}
