package edu.neu.madcourse.shresthbhatnagar.com.api;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by vidhu on 3/2/2017.
 */

public class LeaderBoardApi {

    private FirebaseDatabase db = FirebaseDatabase.getInstance();

    public void publishScore(final String username, final int score){
        final DatabaseReference leaderboard = db.getReference("leaderboard");
        leaderboard.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<LeaderBoardEntry> items = new ArrayList<LeaderBoardEntry>();

                //Add existing entries
                for(DataSnapshot entry : dataSnapshot.getChildren()){
                    LeaderBoardEntry le = entry.getValue(LeaderBoardEntry.class);
                    items.add(le);
                }

                //Add new entry
                LeaderBoardEntry entry = new LeaderBoardEntry(username, score);
                items.add(entry);
                Collections.sort(items);
                Collections.reverse(items);

                //Delete old list
                leaderboard.setValue(null);

                for(LeaderBoardEntry le : items){
                    leaderboard.push().setValue(le);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void setLeaderboardItemListener(ValueEventListener listener){
        db.getReference("leaderboard").addValueEventListener(listener);
    }

    public static class LeaderBoardEntry implements Comparable<LeaderBoardEntry>{
        public String username;
        public Integer score;

        public LeaderBoardEntry(){

        }

        public LeaderBoardEntry(String username, int score) {
            this.username = username;
            this.score = score;
        }

        @Override
        public int compareTo(LeaderBoardEntry o) {
            return this.score.compareTo(o.score);
        }

        @Override
        public String toString() {
            return username + ": " + score;
        }
    }
}
