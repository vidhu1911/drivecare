package edu.neu.madcourse.shresthbhatnagar.com.api;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.IgnoreExtraProperties;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by vidhu on 3/1/2017.
 */

public class GameApi {
    public static final String PROP_GAMEID = "gameid";
    public static final String NOTIFICATION = "notification";
    public static final String DATA = "data";
    public static final String EVENT = "event";
    public static final String EVENT_STARTGAME = "game_start";
    public static final String EVENT_MOVE = "game_move";
    public static final String EVENT_END = "game_end";

    private FirebaseDatabase db = FirebaseDatabase.getInstance();

    private OnGameStartListener onGameStartListener;

    public void startGameWithUser(UserApi.User op) {
        //Create Game Object
        Game game = new Game();
        game.player1Name = Api.getInstance().getUserApi().getCurrentUsername();
        game.player2Name = op.username;
        game.player1Id = Api.getInstance().getUserApi().getCurrentUserId();
        game.player2Id = op.id;
        game.isPlayer1Turn = true;
        game.board = "123123123123";

        DatabaseReference gameRef = db.getReference("game").push();
        String gameId = gameRef.getKey();
        gameRef.setValue(game);

        if(this.onGameStartListener != null) {
            this.onGameStartListener.gameStart(gameId);
            Map<String, Object> data = new HashMap<>();
            data.put(PROP_GAMEID, gameId);
            data.put(EVENT, EVENT_STARTGAME);
            data.put(DATA, gameId);
            Scroggle2MessagingService.sendMessage(op.id, data);
        }
    }

    public void getCurrentGame(ValueEventListener eventListener){
        DatabaseReference game = db.getReference("game");
        game.addListenerForSingleValueEvent(eventListener);
    }

    public void setOnGameUpdateListener(String gameid, ValueEventListener eventListener) {
        db.getReference("game").orderByKey().equalTo(gameid).addValueEventListener(eventListener);
    }

    public void updateGame(String gameId, Game game){
        db.getReference("game").child(gameId).setValue(game);

        Map<String, Object> data = new HashMap<>();
        data.put(EVENT, EVENT_MOVE);

        Map<String, String> notification = new HashMap<>();
        notification.put("title", "Scroggle2");
        notification.put("body", "A new move was made in your game");
        notification.put("sound", "default");
        notification.put("badge", "1");
        notification.put("click_action", "OPEN_PLAYER_LIST");


        Scroggle2MessagingService.sendMessage(game.player1Id, data, notification);
        Scroggle2MessagingService.sendMessage(game.player2Id, data, notification);
    }

    public void endGame(String gameId, Game game) {
        //Get winnder's username
        String winner = game.player1Score < game.player2Score ? game.player2Name : game.player1Name;
        int score = game.player1Score < game.player2Score ? game.player2Score : game.player1Score;

        //Update leaderboard
        Api.getInstance().getLeaderBoardApi().publishScore(winner, score);

        //Send notification to close game
        Map<String, Object> data = new HashMap<>();
        data.put(EVENT, EVENT_END);
        Map<String, String> notification = new HashMap<>();
        notification.put("title", "Scroggle2");
        notification.put("body", winner + " won the game with score: " + score);
        notification.put("sound", "default");
        notification.put("badge", "1");
        notification.put("click_action", "OPEN_PLAYER_LIST");
        Scroggle2MessagingService.sendMessage(game.player1Id, data, notification);
        Scroggle2MessagingService.sendMessage(game.player2Id, data, notification);

        //Delete game
        db.getReference("game").child(gameId).setValue(null);
    }

    public OnGameStartListener getOnGameStartListener() {
        return onGameStartListener;
    }

    public void setOnGameStartListener(OnGameStartListener onGameStartListener) {
        this.onGameStartListener = onGameStartListener;
    }

    public static interface OnGameStartListener {
        void gameStart(String gameId);
    }


    @IgnoreExtraProperties
    public static class Game {
        public String player1Name;
        public String player2Name;
        public String player1Id;
        public String player2Id;
        public int player1Score = 0;
        public int player2Score = 0;
        public boolean isPlayer1Turn;
        public String board;
    }

}
