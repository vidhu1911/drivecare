package edu.neu.madcourse.shresthbhatnagar.scroggle2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import edu.neu.madcourse.shresthbhatnagar.hellomad.R;

public class HowToPlay extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scr2_activity_how_to_play);
        setTitle("How To Play");
    }
}
