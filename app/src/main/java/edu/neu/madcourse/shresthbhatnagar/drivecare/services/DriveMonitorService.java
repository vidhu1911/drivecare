package edu.neu.madcourse.shresthbhatnagar.drivecare.services;


import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.location.LocationListener;

import java.util.Random;

import edu.neu.madcourse.shresthbhatnagar.drivecare.activities.StartMonitor;
import edu.neu.madcourse.shresthbhatnagar.drivecare.activities.TrickyPart;
import edu.neu.madcourse.shresthbhatnagar.drivecare.lib.AccSensor;
import edu.neu.madcourse.shresthbhatnagar.drivecare.lib.LocationSensor;
import edu.neu.madcourse.shresthbhatnagar.drivecare.lib.TripTracker;
import edu.neu.madcourse.shresthbhatnagar.drivecare.lib.WearAccSensor;
import edu.neu.madcourse.shresthbhatnagar.hellomad.R;

/**
 * Created by vidhu on 4/18/2017.
 */

public class DriveMonitorService extends Service implements LocationListener, AccSensor.AccSensorEventListener, WearAccSensor.OnWearMesurement {
    private static final String TAG = DriveMonitorService.class.getSimpleName();

    private final int NOTIFICATION_ID = 1;
    private final IBinder binder = new LocalBinder();
    private final Random random = new Random();

    //Data
    private NotificationManagerCompat nm;
    private NotificationCompat.Builder nb;
    private TripTracker tripTracker = new TripTracker(this);
    private BroadcastReceiver receiver;

    //Sensors
    private AccSensor accSensor;
    private LocationSensor locationSensor;
    private WearAccSensor wearAccSensor;

    @Override
    public void onCreate() {
        super.onCreate();

        //Init Stuff
         this.nm = NotificationManagerCompat.from(this);

        //Create Intent and Foreground notificaiton
        Intent notificationIntent = new Intent(this, StartMonitor.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
        this.nb = new NotificationCompat.Builder(this)
                .setContentTitle(getString(R.string.dc_notification_title))
                .setContentText(getString(R.string.dc_notification_text))
                .setSmallIcon(R.drawable.car_map_marker)
                .setContentIntent(pendingIntent)
                .setTicker(getText(R.string.dc_notification_text));
        startForeground(NOTIFICATION_ID, this.nb.build());

        //Init Sensors
        this.locationSensor = new LocationSensor(this, this);
        this.accSensor = new AccSensor(this, 5.0f, 5.0f, 5.0f);
        this.accSensor.addListener(this);
        this.wearAccSensor = new WearAccSensor(this);
        this.wearAccSensor.registerListener(this);

        //Start Sensors
        this.locationSensor.start();
        this.accSensor.startSensor();
        this.wearAccSensor.start();

        //Broadcast Receiver
        IntentFilter filter = new IntentFilter();
        filter.addAction("trip.ignore");
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(intent.getAction().equals("trip.ignore")){
                    int notificationID = intent.getIntExtra("ID", -1);
                    if(notificationID != -1)
                        nm.cancel(notificationID);
                    Log.d(TAG, "Ignoring this event with time: " + intent.getLongExtra("TIME", Long.valueOf(12)));
                    long time = intent.getLongExtra("TIME", -1L);
                    if(time != -1L)
                        DriveMonitorService.this.tripTracker.dismissTripPoint(time);
                }
            }
        };

        registerReceiver(receiver, filter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
        this.locationSensor.stop();
        this.accSensor.stopSensor();
        this.wearAccSensor.stop();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    public int getRandomNumber() {
        int num = random.nextInt(100);
        this.nb.setContentTitle(String.valueOf(num));
        startForeground(NOTIFICATION_ID, this.nb.build());
        return num;
    }

    public void stopMonitoring() {
        Toast.makeText(this, "Your driving stats has been saved", Toast.LENGTH_LONG).show();
        this.tripTracker.saveToSDCard();
        stopForeground(true);
        stopSelf();
    }

    public void cancelMonitoring() {
        Toast.makeText(this, "Drive monitoring has been canceled", Toast.LENGTH_LONG).show();
        stopForeground(true);
        stopSelf();
    }

    public class LocalBinder extends Binder {
        public DriveMonitorService getService() {
            return DriveMonitorService.this;
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if(this.tripTracker != null)
            this.tripTracker.addPoint(location);
    }

    @Override
    public void onAccelerationEvent(int event, float timeStamp) {
        TripTracker.TripPoint tripPoint = this.tripTracker.addPoint(event);
        if(tripPoint != null)
            showNotification(event, tripPoint.getTime());
    }

    @Override
    public void OnWearMeasurement(long timestamp, float[] values) {
        this.tripTracker.addWearPoint(new TripTracker.Vector3D(values[0], values[1], values[2]));
    }

    public void showNotification(int event, long time) {
        Intent ignoreIntent = new Intent("trip.ignore");
        ignoreIntent.putExtra("TIME", Long.valueOf(time));
        ignoreIntent.putExtra("ID", 2);
        PendingIntent ignorePendingIntent = PendingIntent.getBroadcast(this, 0, ignoreIntent, PendingIntent.FLAG_UPDATE_CURRENT);


        Bitmap wearbg = BitmapFactory.decodeResource(this.getResources(), R.drawable.notification_bg);

        NotificationCompat.Builder nb = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Driving Alert")
                .setDefaults(Notification.DEFAULT_ALL)
                .setPriority(Notification.PRIORITY_MAX)
                .addAction(R.drawable.check, "Ignore", ignorePendingIntent);

        NotificationCompat.WearableExtender wearableExtender =  new NotificationCompat.WearableExtender();
        wearableExtender.setBackground(wearbg);

        switch (event){
            case AccSensor.EVENT_TURN_LEFT:
                nb.setContentText("Hard Left Turn detected");
                wearableExtender.setContentIcon(R.drawable.dc_event_left);
                break;
            case AccSensor.EVENT_TURN_RIGHT:
                nb.setContentText("Hard Right Turn detected");
                wearableExtender.setContentIcon(R.drawable.dc_event_right);
                break;
            case AccSensor.EVENT_ACCELERATION:
                nb.setContentText("Hard Acceleration detected");
                wearableExtender.setContentIcon(R.drawable.dc_event_acc);
                break;
            case AccSensor.EVENT_BREAK:
                nb.setContentText("Hard Braking detected");
                wearableExtender.setContentIcon(R.drawable.dc_event_brake);
                break;
        }
        nb.extend(wearableExtender);

        this.nm.notify(2, nb.build());
    }
}
