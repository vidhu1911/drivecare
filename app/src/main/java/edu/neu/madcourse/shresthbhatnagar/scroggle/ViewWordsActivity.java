package edu.neu.madcourse.shresthbhatnagar.scroggle;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Arrays;

import edu.neu.madcourse.shresthbhatnagar.hellomad.R;

public class ViewWordsActivity extends AppCompatActivity {

    ListView lv_scrWords;

    ArrayAdapter adapter;
    ArrayList<String> wordsFound = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scroggle_view_words);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        String[] wordsFoundArray = getIntent().getStringArrayExtra(GameActivity.WORD_LIST);
        this.wordsFound = new ArrayList<String>(Arrays.asList(wordsFoundArray));

        lv_scrWords = (ListView) findViewById(R.id.lv_scrWords);
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, this.wordsFound);
        this.lv_scrWords.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
