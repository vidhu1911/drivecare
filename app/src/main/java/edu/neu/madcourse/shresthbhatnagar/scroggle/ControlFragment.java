package edu.neu.madcourse.shresthbhatnagar.scroggle;


import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import edu.neu.madcourse.shresthbhatnagar.hellomad.R;


public class ControlFragment extends Fragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_scroggle_control, container, false);


        View restart = rootView.findViewById(R.id.button_scrRestart);
        View main = rootView.findViewById(R.id.button_scrMain);
        View submit = rootView.findViewById(R.id.button_scrCheck);
        View pause = rootView.findViewById(R.id.button_scrPause);

        restart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((GameActivity) getActivity()).unselectAll();
            }
        });

        main.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                getActivity().finish();
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((GameActivity) getActivity()).submitWord();
            }
        });

        pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((GameActivity) getActivity()).pauseGame();
            }
        });
        return rootView;
    }
}
