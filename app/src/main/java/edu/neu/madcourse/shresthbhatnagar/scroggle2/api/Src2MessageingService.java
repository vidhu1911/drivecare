package edu.neu.madcourse.shresthbhatnagar.scroggle2.api;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONObject;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;
import java.util.Scanner;

/**
 * Created by vidhu on 3/15/2017.
 */

public class Src2MessageingService extends FirebaseMessagingService {
    private static final String TAG = Src2MessageingService.class.getSimpleName();
    private static final String SERVER_KEY = "key=AAAAqxwTMu8:APA91bEa5pMEM4ET0hjvRtl499r1N8B7gIaW-TYWqh3WXYTXufZiSOoxUWohFPn23NTPmpyTrJetgAbCYKYWotSzViIemUsRePfUboWuS3EJ3Xhq50LB8of2Y9aynwZa2PqsAawtwAtj";

    public static void sendNotification(String to, Map<String, String> notification){
        if(!notification.containsKey("title"))
            notification.put("title", "2 Player Word Game");
        if(!notification.containsKey("sound"))
            notification.put("sound", "default");
        if(!notification.containsKey("badge"))
            notification.put("badge", "1");
        if(!notification.containsKey("click_action"))
            notification.put("click_action", "OPEN_PLAYER_LIST_SCR2");

        sendMessage(to, null, notification);
    }

    public static void sendMessage(String to, final Map<String, Object> data, final Map<String, String> notification){
        final String _to = to;
        final JSONObject _data = data == null ? null : new JSONObject(data);
        final JSONObject _notification = notification == null ? null : new JSONObject(notification);

        new Thread(new Runnable() {
            @Override
            public void run() {
                JSONObject jPayload = new JSONObject();

                try {
                    jPayload.put("to", _to);
                    jPayload.put("priority", "high");

                    if(_notification != null)
                        jPayload.put("notification", _notification);

                    if(_data != null)
                        jPayload.put("data", _data);

                    URL url = new URL("https://fcm.googleapis.com/fcm/send");
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Authorization", SERVER_KEY);
                    conn.setRequestProperty("Content-Type", "application/json");
                    conn.setDoOutput(true);

                    // Send FCM message content.
                    OutputStream outputStream = conn.getOutputStream();
                    outputStream.write(jPayload.toString().getBytes());
                    outputStream.close();

                    // Read FCM response.
                    InputStream inputStream = conn.getInputStream();
                    final String resp = convertStreamToString(inputStream);

                    Handler h = new Handler(Looper.getMainLooper());
                    h.post(new Runnable() {
                        @Override
                        public void run() {
                            Log.e("GAMEApi", "run: " + resp);
                        }
                    });

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }).start();
    }

    private static String convertStreamToString(InputStream is) {
        Scanner s = new Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next().replace(",", ",\n") : "";
    }
}
