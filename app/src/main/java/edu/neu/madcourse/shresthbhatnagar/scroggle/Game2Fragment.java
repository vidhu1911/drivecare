package edu.neu.madcourse.shresthbhatnagar.scroggle;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;

import java.util.ArrayList;
import java.util.List;

import edu.neu.madcourse.shresthbhatnagar.hellomad.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class Game2Fragment extends Fragment {
    final static String LETTERS = "letters";

    GridLayout gridBoard;
    int boardLen;

    String[] letters;
    private TileLetter mSmallTiles[];
    private String currentWord = "";
    private int score = 0;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initGame();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_scroggle_game2, container, false);
        initViews(view);
        return view;
    }

    public Game2Fragment seedLetters(String[] letters){
        this.letters = letters;
        return this;
    }

    protected void initGame(){
        if(this.letters == null || this.letters.length == 0)
            return;

        int sqrLen = (int) Math.ceil(Math.sqrt(this.letters.length));
        this.boardLen = sqrLen;
        this.mSmallTiles = new TileLetter[sqrLen*sqrLen];
        for(int small=0; small<sqrLen*sqrLen; small++){
            this.mSmallTiles[small] = new TileLetter();
            if(small < this.letters.length)
                this.mSmallTiles[small].setLetter(this.letters[small]);
        }
    }

    protected void initViews(View rootView) {
        if(this.letters == null || this.letters.length == 0)
            return;

        int sqrLen = (int) Math.ceil(Math.sqrt(this.letters.length));

        //Adjust GridView
        this.gridBoard = (GridLayout) rootView.findViewById(R.id.game2_board);
        this.gridBoard.setColumnCount(sqrLen);
        this.gridBoard.setRowCount(sqrLen);

        //Add Tiles
        for(int row=0; row<sqrLen; row++){
            for(int col=0; col<sqrLen; col++){
                int idx = (sqrLen*row)+col;

                LetterTileView inner = new LetterTileView(rootView.getContext(), null);
                final int fSmall = idx;
                inner.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        makeMove(fSmall);
                    }
                });
                this.mSmallTiles[idx].setView(inner);

                GridLayout.LayoutParams layout = new GridLayout.LayoutParams();
                layout.rowSpec = GridLayout.spec(row, GridLayout.CENTER);
                layout.columnSpec = GridLayout.spec(col, GridLayout.CENTER);
                this.gridBoard.addView(inner, layout);
            }
        }
    }

    protected void makeMove(int idx){
        if (!this.mSmallTiles[idx].isActive())
            return;

        this.currentWord += this.mSmallTiles[idx].getLetter();
        setActive(idx);
        updateAllTiles();
        updateCurrentWord();
    }

    protected void setActive(int idx) {
        this.deactivateAll();

        this.mSmallTiles[idx].setChoosen(true);

        int top = idx-boardLen;
        int topl = top-1;
        int topr = top+1;
        int right = idx+1;
        int bot = idx+boardLen;
        int botr = bot+1;
        int botl = bot-1;
        int left = idx-1;

        List<Integer> pos = new ArrayList<>();
        if(top>=0)
            pos.add(top);
        if(bot<this.mSmallTiles.length)
            pos.add(bot);
        if(left>= 0 && left % boardLen != boardLen-1)
            pos.add(left);
        if(right< this.mSmallTiles.length && right % boardLen != 0)
            pos.add(right);
        if(topl >= 0 && topl % boardLen != boardLen-1)
            pos.add(topl);
        if(topr >= 0 && right % boardLen != 0)
            pos.add(topr);
        if(botl < this.mSmallTiles.length && botl % boardLen != boardLen-1)
            pos.add(botl);
        if(botr < this.mSmallTiles.length && botr % boardLen != 0)
            pos.add(botr);

        for(Integer i : pos){
            this.mSmallTiles[i].setActive(true);
        }
    }

    protected void deactivateAll() {
        for(int small=0; small<this.mSmallTiles.length; small++){
            this.mSmallTiles[small].setActive(false);
        }
    }

    protected void updateAllTiles() {
        for(int small=0; small<this.mSmallTiles.length; small++){
            this.mSmallTiles[small].updateDrawableState();
        }
    }

    public void unselectAll() {
        this.currentWord = "";
        this.updateCurrentWord();
        for(int small=0; small<this.mSmallTiles.length; small++){
            TileLetter tileLetter = this.mSmallTiles[small];
            this.mSmallTiles[small].setChoosen(false);
            if(!tileLetter.getLetter().equals(""))
                this.mSmallTiles[small].setActive(true);
        }
        this.updateAllTiles();
    }

    public void finishSmall() {
        for(int small=0; small<this.mSmallTiles.length; small++){
            TileLetter tileLetter = this.mSmallTiles[small];
            if(tileLetter.isChoosen())
                tileLetter.setLetter("");
            this.mSmallTiles[small].setActive(false);
        }
        this.unselectAll();
    }

    protected void updateCurrentWord() {
        ((GameActivity) getActivity()).updateCurrentWord(this.currentWord);
    }

    public String getCurrentWord() {
        return currentWord;
    }

    public void setCurrentWord(String currentWord) {
        this.currentWord = currentWord;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getState() {
        StringBuilder builder = new StringBuilder();
        builder.append(currentWord);
        builder.append(",");
        builder.append(this.score);
        builder.append(",");

        for(int small=0; small<this.mSmallTiles.length; small++){
            int isActive = this.mSmallTiles[small].isActive() ? 1 : 0;
            int isChoosen = this.mSmallTiles[small].isChoosen() ? 1 : 0;
            builder.append(isActive);
            builder.append(",");
            builder.append(isChoosen);
            builder.append(",");
            builder.append(this.mSmallTiles[small].getLetter());
            builder.append(",");
        }
        return builder.toString();
    }

    public void putState(String gameData) {
        String[] fields = gameData.split(",");

        int idx = 0;
        this.currentWord = fields[idx++];
        this.score = Integer.valueOf(fields[idx++]);


        List<Boolean> stateActive = new ArrayList<>();
        List<Boolean> stateChoosen = new ArrayList<>();
        List<String> letters = new ArrayList<>();
        for(;idx<fields.length;){
            stateActive.add(fields[idx++].equals("1"));
            stateChoosen.add(fields[idx++].equals("1"));
            letters.add(fields[idx++]);
        }

        this.letters = letters.toArray(new String[0]);
        initGame();
        initViews(getView().getRootView());
        for(int i=0; i<this.mSmallTiles.length; i++){
            this.mSmallTiles[i].setActive(stateActive.get(i));
            this.mSmallTiles[i].setChoosen(stateChoosen.get(i));
        }

        this.updateAllTiles();
    }

}
