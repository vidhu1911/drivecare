package edu.neu.madcourse.shresthbhatnagar.scroggle2.game;

import android.content.DialogInterface;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.GridLayout;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import edu.neu.madcourse.shresthbhatnagar.hellomad.R;
import edu.neu.madcourse.shresthbhatnagar.scroggle2.api.GameApi;
import edu.neu.madcourse.shresthbhatnagar.scroggle2.api.NetworkStateReceiver;
import edu.neu.madcourse.shresthbhatnagar.scroggle2.api.UserApi;

public class GameActivity extends AppCompatActivity implements
        TiltSensor.TiltChangeListener, View.OnClickListener, GameApi.GameStateChangedListener,
        NetworkStateReceiver.NetworkStateReceiverListener{

    //Support
    WordChecker wordChecker;
    TiltSensor tiltSensor;
    NetworkStateReceiver networkStateReceiver;
    SoundPlayer soundPlayer;

    //Data
    GameApi.Game gameState;
    TileLetter tiles[] = new TileLetter[100];
    String gameId;
    boolean isPlayer1;
    String currWord = "";
    int currScore = 0;

    //Views
    TextView tv_word;
    TextView tv_pTurn;
    TextView tv_player1Name;
    TextView tv_player2Name;
    TextView tv_score1;
    TextView tv_score2;
    GridLayout grd_gameboard;
    ImageButton btn_unselect;
    ImageButton btn_submit;
    ImageButton btn_exit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scr2_activity_game);
        setTitle("Two Player Word Game");

        //Init support and Data
        this.networkStateReceiver = new NetworkStateReceiver();
        this.networkStateReceiver.addListener(this);
        this.wordChecker = new WordChecker(this.getAssets());
        this.tiltSensor = new TiltSensor(this, 6);
        this.tiltSensor.addListener(this);
        this.soundPlayer = new SoundPlayer(this);
        this.gameId = this.getIntent().getStringExtra("GAMEID");
        GameApi.setGameStateChangedListener(this);


        //Init all others
        initViews();
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.tiltSensor.startSensor();
        this.registerReceiver(this.networkStateReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    @Override
    protected void onPause() {
        super.onPause();
        this.tiltSensor.stopSensor();
        this.unregisterReceiver(this.networkStateReceiver);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_src2_reset:
                this.soundPlayer.playClick();
                this.unselectAll();
                break;
            case R.id.btn_src2_submit:
                this.soundPlayer.playClick();
                this.submitWord();
                break;
            case R.id.btn_src2_giveup:
                this.endGame();
                break;
        }
    }

    @Override
    public void onTiltChange(int direction) {
        Log.d("Tilt: ", String.valueOf(direction));
        tiltBoard(direction);
        initViews();
    }

    @Override
    public String getGameId() {
        return this.gameId;
    }

    @Override
    public void gameStateChanged(GameApi.Game game) {
        //Save GameState
        this.gameState = game;

        //Check if game has ended
        if(game.isEnd)
            this.showEndGameDialog();

        //Set player number
        this.isPlayer1 = game.player1Id.equals(UserApi.getCurrentUserId());

        //Create Game Board
        this.initGame(game.board);

        //Set players' scores
        this.tv_score1.setText(String.valueOf(game.player1Score));
        this.tv_score2.setText(String.valueOf(game.player2Score));
        if(this.isPlayer1)
            this.currScore = this.gameState.player1Score;
        else
            this.currScore = this.gameState.player2Score;

        //Set players' names
        this.tv_player1Name.setText(game.player1Name);
        this.tv_player2Name.setText(game.player2Name);

        //Update controls depending on player's turn
        if(this.isMyTurn()) {
            this.tv_pTurn.setText("Your turn");
            this.btn_submit.setEnabled(true);
            this.btn_unselect.setEnabled(true);
        }else{
            this.tv_pTurn.setText("Opponent's turn");
            this.deactivateAll();
            this.btn_submit.setEnabled(false);
            this.btn_unselect.setEnabled(false);
        }
    }


    private void initGame(String board) {
        String[] letters = board.split(",", -1);
        for (int i = 0; i < this.tiles.length; i++) {
            String letter = letters[i];
            TileLetter tileLetter = new TileLetter();
            tileLetter.setLetter(letter);
            this.tiles[i] = tileLetter;
        }

        //Draw views for game board
        drawBoard();
    }

    private void initViews() {
        //TextViews
        this.tv_word = (TextView) findViewById(R.id.tv_src2_word);
        this.tv_pTurn = (TextView) findViewById(R.id.tv_src2_turn);
        this.tv_player1Name = (TextView) findViewById(R.id.tv_src2_name1);
        this.tv_player2Name = (TextView) findViewById(R.id.tv_src2_name2);
        this.tv_score1 = (TextView) findViewById(R.id.tv_src2_score1);
        this.tv_score2 = (TextView) findViewById(R.id.tv_src2_score2);

        //Control buttons
        this.btn_unselect = (ImageButton) findViewById(R.id.btn_src2_reset);
        this.btn_submit = (ImageButton) findViewById(R.id.btn_src2_submit);
        this.btn_exit = (ImageButton) findViewById(R.id.btn_src2_giveup);

        //Set Control buttons' listeners
        this.btn_unselect.setOnClickListener(this);
        this.btn_submit.setOnClickListener(this);
        this.btn_exit.setOnClickListener(this);

        //GridView
        this.grd_gameboard = (GridLayout) findViewById(R.id.grd_src2board);
        this.grd_gameboard.setColumnCount(10);
        this.grd_gameboard.setRowCount(10);
    }


    private void drawBoard() {
        //Add Tiles
        for (int row = 0; row < 10; row++) {
            for (int col = 0; col < 10; col++) {
                int idx = (10 * row) + col;

                LetterTileView view = new LetterTileView(this, null);
                this.tiles[idx].setView(view);
                final int tileIdx = idx;
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        makeMove(tileIdx);
                    }
                });


                GridLayout.LayoutParams layout = new GridLayout.LayoutParams();
                layout.rowSpec = GridLayout.spec(row, GridLayout.CENTER);
                layout.columnSpec = GridLayout.spec(col, GridLayout.CENTER);
                this.grd_gameboard.addView(view, layout);
            }
        }
    }

    private void tiltBoard(int tilt) {
        if(!this.isMyTurn())
            return;

        this.grd_gameboard.removeAllViews();
        for (int row = 0; row < 10; row++) {
            String letters = "";

            for (int col = 0; col < 10; col++) {
                int idx = (10 * row) + col;
                letters += this.tiles[idx].getLetter();
            }

            int N = letters.length();
            if (N == 10)
                continue;

            if (tilt == 1) {
                for (int col = 0; col < N; col++) {
                    int idx = (10 * row) + col;

                    TileLetter tileLetter = new TileLetter();
                    tileLetter.setLetter(String.valueOf(letters.charAt(col)));
                    this.tiles[idx] = tileLetter;
                }
                for (int col = N; col < 10; col++) {
                    int idx = (10 * row) + col;

                    TileLetter tileLetter = new TileLetter();
                    tileLetter.setLetter("");
                    this.tiles[idx] = tileLetter;
                }
            } else if (tilt == -1) {
                for (int col = 0; col < 10 - N; col++) {
                    int idx = (10 * row) + col;

                    TileLetter tileLetter = new TileLetter();
                    tileLetter.setLetter("");
                    this.tiles[idx] = tileLetter;
                }
                for (int col = 10 - N; col < 10; col++) {
                    int idx = (10 * row) + col;

                    TileLetter tileLetter = new TileLetter();
                    tileLetter.setLetter(String.valueOf(letters.charAt(col - (10 - N))));
                    this.tiles[idx] = tileLetter;
                }
            }
        }
        this.drawBoard();
        this.updateGameState();
    }

    private boolean isMyTurn(){
        return (this.gameState.isPlayer1Turn && this.isPlayer1) ||
                (!this.gameState.isPlayer1Turn && !this.isPlayer1);
    }

    private void makeMove(int idx) {
        if (!this.tiles[idx].isActive())
            return;

        //Play Sound
        this.soundPlayer.playClick();

        this.setActive(idx);
        this.updateAllTiles();
        this.updateCurrentWord(this.currWord + this.tiles[idx].getLetter());
    }

    private void setActive(int idx) {
        this.deactivateAll();

        this.tiles[idx].setChoosen(true);

        int boardLen = 10;
        int top = idx - boardLen;
        int topl = top - 1;
        int topr = top + 1;
        int right = idx + 1;
        int bot = idx + boardLen;
        int botr = bot + 1;
        int botl = bot - 1;
        int left = idx - 1;

        List<Integer> pos = new ArrayList<>();
        if (top >= 0)
            pos.add(top);
        if (bot < this.tiles.length)
            pos.add(bot);
        if (left >= 0 && left % boardLen != boardLen - 1)
            pos.add(left);
        if (right < this.tiles.length && right % boardLen != 0)
            pos.add(right);
        if (topl >= 0 && topl % boardLen != boardLen - 1)
            pos.add(topl);
        if (topr >= 0 && right % boardLen != 0)
            pos.add(topr);
        if (botl < this.tiles.length && botl % boardLen != boardLen - 1)
            pos.add(botl);
        if (botr < this.tiles.length && botr % boardLen != 0)
            pos.add(botr);

        for (Integer i : pos) {
            TileLetter tile = this.tiles[i];
            if (!tile.getLetter().equals("") && !tile.isChoosen())
                tile.setActive(true);
        }
    }

    private void deactivateAll() {
        for (int i = 0; i < 100; i++)
            this.tiles[i].setActive(false);
    }

    private void updateAllTiles() {
        for (int i = 0; i < 100; i++)
            this.tiles[i].updateDrawableState();
    }

    private void unselectAll() {
        this.updateCurrentWord("");

        for (int small = 0; small < this.tiles.length; small++) {
            TileLetter tileLetter = this.tiles[small];
            this.tiles[small].setChoosen(false);
            if (!tileLetter.getLetter().equals(""))
                this.tiles[small].setActive(true);
        }
        this.updateAllTiles();
    }

    private void removeSelected() {
        for (int i = 0; i < this.tiles.length; i++) {
            TileLetter tileLetter = this.tiles[i];
            if (tileLetter.isChoosen())
                tileLetter.setLetter("");
        }
        this.unselectAll();
    }

    private void updateCurrentWord(String word) {
        if (word.length() == 1)
            this.wordChecker.loadWords(word);

        this.currWord = word;
        this.tv_word.setText(this.currWord);
    }

    private void updateCurrentScore(int score) {
        this.currScore = score;
        this.tv_score1.setText(String.valueOf(this.currScore));
    }

    private void submitWord() {
        String word = this.tv_word.getText().toString();

        if (word.length() < 3)
            return;

        if (this.wordChecker.isWord(word)) {
            this.soundPlayer.playSuccess();
            this.updateCurrentScore(this.currScore + (word.length() * 10));
            this.removeSelected();
        } else
            this.updateCurrentScore(this.currScore - 10);

        this.unselectAll();
        this.gameState.isPlayer1Turn = !this.gameState.isPlayer1Turn;
        this.updateGameState();
    }

    private String boardToString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < this.tiles.length; i++) {
            sb.append(this.tiles[i].getLetter());
            sb.append(",");
        }
        return sb.toString();
    }

    private void updateGameState() {
        this.gameState.board = this.boardToString();
        if(this.isPlayer1)
            this.gameState.player1Score = this.currScore;
        else
            this.gameState.player2Score = this.currScore;

        GameApi.updateGame(this.getGameId(), this.gameState);
    }

    private void showEndGameDialog() {
        String msg = this.gameState.getWinner() + " won with the score of " + this.gameState.getHighestScore();
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
        finish();
    }

    private void endGame() {
        GameApi.removeGameStateChangedListener();
        GameApi.endGame(this.getGameId(), this.gameState);
        this.showEndGameDialog();
    }

    @Override
    public void networkAvailable() {

    }

    @Override
    public void networkUnavailable() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Network unavailable. You can still play your move and it will be synced once network connectivity is restored")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //do things
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }
}
