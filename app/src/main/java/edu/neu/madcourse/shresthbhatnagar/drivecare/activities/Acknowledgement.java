package edu.neu.madcourse.shresthbhatnagar.drivecare.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

import edu.neu.madcourse.shresthbhatnagar.hellomad.R;

public class Acknowledgement extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dc_activity_acknowledgement);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        WebView ackWebView = (WebView) findViewById(R.id.wv_ack);
        ackWebView.getSettings().setJavaScriptEnabled(true);
        ackWebView.loadData(getString(R.string.finalProjectAck), "text/html", null);
    }
}
