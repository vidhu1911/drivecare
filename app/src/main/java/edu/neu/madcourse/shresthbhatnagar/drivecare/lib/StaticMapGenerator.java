package edu.neu.madcourse.shresthbhatnagar.drivecare.lib;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vidhu on 4/19/2017.
 */

public class StaticMapGenerator {
    private static final String TAG = StaticMapGenerator.class.getSimpleName();
    private static final String BASE_URL = "https://maps.googleapis.com/maps/api/staticmap?size=600x300&maptype=roadmap&path=";

    private Context context;
    private List<Location> locations = new ArrayList<>();


    public StaticMapGenerator(Context context) {
        this.context = context;
    }

    public void addLocation(Location location) {
        this.locations.add(location);
    }

    public void generateImage(final File file) {
        Log.d(TAG, this.generateURL());

        new DownloadImageFromInternet(file).execute(this.generateURL());

        /**
        Picasso.with(this.context)
                .load(this.generateURL())
                //.load("https://images-na.ssl-images-amazon.com/images/I/41fw65-8tpL._AC_SY200_.jpg")
                .into(new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        FileOutputStream out = null;
                        try {
                            out = new FileOutputStream(file);
                            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
                            out.flush();
                            out.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(context, "Error generating static path image. Check LogCat", Toast.LENGTH_LONG);
                            Log.d(TAG, Log.getStackTraceString(e));
                        }
                    }

                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {
                        Log.d(TAG, "Error Loading Image");
                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {
                        Log.d(TAG, "Preparing Loading Image");
                    }
                });
         **/
    }

    private String generateURL() {
        StringBuilder sb = new StringBuilder(BASE_URL);
        for (Location loc : this.locations) {
            sb.append(loc.getLatitude());
            sb.append(",");
            sb.append(loc.getLongitude());
            sb.append("|");
        }
        sb.setLength(sb.length() - 1);
        return sb.toString();
    }

    private class DownloadImageFromInternet extends AsyncTask<String, Void, Bitmap> {

        File file = null;

        public DownloadImageFromInternet(File file){
            this.file = file;
        }

        protected Bitmap doInBackground(String... urls) {
            String imageURL = urls[0];

            Bitmap bimage = null;
            try {
                InputStream in = new java.net.URL(imageURL).openStream();
                bimage = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error Message", Log.getStackTraceString(e));
            }

            return bimage;
        }

        protected void onPostExecute(Bitmap result) {
            FileOutputStream out = null;
            try {
                out = new FileOutputStream(file);
                result.compress(Bitmap.CompressFormat.PNG, 100, out);
                out.flush();
                out.close();
            } catch (Exception e) {
                Log.d(TAG, Log.getStackTraceString(e));
            }

        }
    }
}
