package edu.neu.madcourse.shresthbhatnagar.com.test;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Map;

import edu.neu.madcourse.shresthbhatnagar.hellomad.R;
import edu.neu.madcourse.shresthbhatnagar.com.api.Api;
import edu.neu.madcourse.shresthbhatnagar.com.api.GameApi;
import edu.neu.madcourse.shresthbhatnagar.com.api.Scroggle2MessagingService;
import edu.neu.madcourse.shresthbhatnagar.com.api.UserApi;

public class PlayerListActivity extends AppCompatActivity implements Scroggle2MessagingService.OnMessageReceiveListener{

    EditText et_username;
    SwitchCompat switch_available;
    ListView lv_availableUsers;
    Button btn_leaderboard;

    ArrayList<UserApi.User> availableUsers;
    ArrayAdapter<UserApi.User> availableUsersAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_activity_player_list);

        //Init data objects
        availableUsers = new ArrayList<>();
        availableUsersAdapter = new ArrayAdapter<UserApi.User>(this, android.R.layout.simple_list_item_1, availableUsers);

        //Init references to views
        this.et_username = (EditText) findViewById(R.id.et_test_username);
        this.switch_available = (SwitchCompat) findViewById(R.id.switch_test_available);
        this.lv_availableUsers = (ListView) findViewById(R.id.lv_test_availableUsers);
        this.btn_leaderboard = (Button) findViewById(R.id.btn_test_leaderboard);

        //Init view properties
        this.et_username.setText(this.getSavedUsername());
        this.lv_availableUsers.setAdapter(this.availableUsersAdapter);

        //Init event listeners
        Scroggle2MessagingService.addOnMessageReceiveListener(this);

        this.switch_available.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    if (et_username.getText().length() == 0) {
                        Toast.makeText(compoundButton.getContext(), "Enter a username", Toast.LENGTH_SHORT)
                                .show();
                        switch_available.setChecked(false);
                    } else {
                        setUserAvailable();
                    }
                } else {
                    setUserOffline();
                }
            }
        });

        this.lv_availableUsers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {
                UserApi.User user = availableUsers.get(pos);
                if(user.id.equals(Api.getInstance().getUserApi().getCurrentUserId())) {
                    Toast.makeText(view.getContext(), "Can't start a game with yourself", Toast.LENGTH_SHORT)
                            .show();
                }else{
                    Toast.makeText(view.getContext(), "User: " + user.id, Toast.LENGTH_LONG).show();
                    Api.getInstance().getGameApi().startGameWithUser(user);
                }
            }
        });

        this.btn_leaderboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), Leaderboard.class);
                startActivity(intent);
            }
        });

        Api.getInstance().getUserApi().setAvailableUsersListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                availableUsers.clear();

                for (DataSnapshot user : dataSnapshot.getChildren()) {
                    UserApi.User u = user.getValue(UserApi.User.class);
                    availableUsers.add(u);
                }

                availableUsersAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        Api.getInstance().getGameApi().getCurrentGame(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot g : dataSnapshot.getChildren()){
                    GameApi.Game game = g.getValue(GameApi.Game.class);
                    String userId = Api.getInstance().getUserApi().getCurrentUserId();
                    if(game.player1Id.equals(userId) || game.player2Id.equals(userId))
                        startGameWithId(g.getKey());
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        Api.getInstance().getGameApi().setOnGameStartListener(new GameApi.OnGameStartListener() {
            @Override
            public void gameStart(String gameId) {
                startGameWithId(gameId);
            }
        });

        //Execute Setup methods
        setUserOffline();
    }

    @Override
    protected void onPause() {
        super.onPause();
        this.switch_available.setChecked(false);
        setUserOffline();
        saveUsername(this.et_username.getText().toString());
    }

    protected void resumeGameIfInGame(){

    }

    protected String getSavedUsername(){
        return this.getPreferences(MODE_PRIVATE).getString("username", "");
    }

    protected void saveUsername(String username){
        this.getPreferences(MODE_PRIVATE).edit()
                .putString("username", username)
                .commit();
    }

    protected void setUserAvailable() {
        UserApi.User user = new UserApi.User(et_username.getText().toString(), UserApi.STATUS_ONLINE);
        Api.getInstance().getUserApi().saveUser(user);
    }

    protected void setUserOffline() {
        String username = et_username.getText().toString();
        if(username.length() == 0) return;
        UserApi.User user = new UserApi.User(et_username.getText().toString(), UserApi.STATUS_OFFLINE);
        Api.getInstance().getUserApi().saveUser(user);
    }

    public void startGameWithId(String gameId){
        Intent intent = new Intent(getBaseContext(), GamePlay.class);
        intent.putExtra(GameApi.PROP_GAMEID, gameId);
        startActivity(intent);
    }

    @Override
    public void onMessage(Map<String, String> data) {
        String action = data.get(GameApi.EVENT);
        if(action.equals(GameApi.EVENT_STARTGAME)){
            String gameId = data.get(GameApi.PROP_GAMEID);
            startGameWithId(gameId);
        }
    }
}
