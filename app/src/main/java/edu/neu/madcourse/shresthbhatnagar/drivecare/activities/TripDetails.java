package edu.neu.madcourse.shresthbhatnagar.drivecare.activities;

import android.location.Location;
import android.os.Environment;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import edu.neu.madcourse.shresthbhatnagar.drivecare.adapters.EventListAdapter;
import edu.neu.madcourse.shresthbhatnagar.drivecare.lib.AccSensor;
import edu.neu.madcourse.shresthbhatnagar.drivecare.lib.TripTracker;
import edu.neu.madcourse.shresthbhatnagar.hellomad.R;

public class TripDetails extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMapLoadedCallback, AdapterView.OnItemClickListener {
    private static final String TAG = TripDetails.class.getSimpleName();

    //Views
    private SupportMapFragment mapFragment;
    private GoogleMap googleMap;
    private TextView tvScore;
    private ListView lvEventlist;

    //Data
    private List<TripTracker.TripPoint> tripPoints = new ArrayList<>();
    private List<TripTracker.TripPoint> eventPoints = new ArrayList<>();
    private String filename;
    private int score = 100;

    //Objects
    EventListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dc_activity_trip_detail);
        setTitle("Tip Details");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        this.filename = getIntent().getStringExtra("FILENAME");
        Log.d("DETAILS", filename);

        this.adapter = new EventListAdapter(this, eventPoints);

        initViews();
    }

    protected void initViews() {
        this.mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        this.mapFragment.getMapAsync(this);
        this.lvEventlist = (ListView) findViewById(R.id.lv_eventlist);
        this.lvEventlist.setAdapter(this.adapter);
        this.lvEventlist.setOnItemClickListener(this);
        this.tvScore = (TextView) findViewById(R.id.tv_score);
    }

    protected void loadData() {
        File path = new File(Environment.getExternalStorageDirectory() + "/drivecare");
        File file = new File(path, filename + ".json");

        StringBuilder text = new StringBuilder();
        try{
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;
            while((line = br.readLine()) != null){
                text.append(line);
                text.append("\n");
            }
            br.close();
        }catch(Exception e) { e.printStackTrace(); }

        List<TripTracker.TripPoint> points = null;
        try {
            JSONObject data = new JSONObject(text.toString());
            Type type = new TypeToken<List<TripTracker.TripPoint>>(){}.getType();
            Gson gson = new Gson();
            points = gson.fromJson(data.getString("trip"), type);
        }catch (Exception e) { Log.d(TAG, Log.getStackTraceString(e)); }


        this.tripPoints.addAll(points);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        googleMap.setOnMapLoadedCallback(this);
        this.loadData();
    }

    @Override
    public void onMapLoaded() {
        PolylineOptions polylineOptions = new PolylineOptions();
        polylineOptions.width(10);
        polylineOptions.clickable(false);

        for(TripTracker.TripPoint point : this.tripPoints){
            polylineOptions.add(point.getLocation());
            if(point.isEvent()) {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("h:m a*");
                MarkerOptions markerOptions = new MarkerOptions().position(point.getLocation());
                markerOptions.snippet(simpleDateFormat.format(point.getTimeasDate()));
                switch (point.getEvent()){
                    case AccSensor.EVENT_ACCELERATION:
                        markerOptions.title("Acceleration");
                        break;
                    case AccSensor.EVENT_BREAK:
                        markerOptions.title("Braking");
                        break;
                    case AccSensor.EVENT_TURN_LEFT:
                        markerOptions.title("Left Turn");
                        break;
                    case AccSensor.EVENT_TURN_RIGHT:
                        markerOptions.title("Right Turn");
                        break;
                }
                this.googleMap.addMarker(markerOptions);
                this.eventPoints.add(point);
                this.adapter.notifyDataSetChanged();
                if(this.score != 0){
                    this.score -= 10;
                    this.tvScore.setText("Score: " + this.score + "%");
                }
            }
        }

        Polyline path = this.googleMap.addPolyline(polylineOptions);


        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for(LatLng latLng : path.getPoints()){
            builder.include(latLng);
        }
        LatLngBounds bounds = builder.build();
        this.googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 50));
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                this.onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
