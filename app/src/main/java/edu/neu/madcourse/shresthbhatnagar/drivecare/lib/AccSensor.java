package edu.neu.madcourse.shresthbhatnagar.drivecare.lib;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vidhu on 4/6/2017.
 */

public class AccSensor implements SensorEventListener {
    private String TAG = AccSensor.class.getSimpleName();
    private SensorManager sensorManager;
    private Sensor sensor;

    private List<AccSensorEventListener> accSensorEventListeners = new ArrayList<>();

    private float turnThresh;
    private float breakThresh;
    private float accThresh;

    private long turnTime = 0;
    private long breakTime = 0;
    private long accTime = 0;

    private long lastEventTime = 0;

    public AccSensor(Context context, float turnThresh, float breakThresh, float accThresh) {
        this.turnThresh = turnThresh;
        this.breakThresh = breakThresh;
        this.accThresh = accThresh;

        this.sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        this.sensor = this.sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
    }

    public void addListener(AccSensorEventListener eventListener){
        this.accSensorEventListeners.add(eventListener);
    }


    public void startSensor() {
        this.sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    public void stopSensor() {
        this.sensorManager.unregisterListener(this);
    }


    @Override
    public void onSensorChanged(SensorEvent event) {
        float sideForce = event.values[0];
        float forwardForce = event.values[2];

        if (Math.abs(sideForce) > this.turnThresh) {
            if(sideForce < 0)
                this.onLeftTurnActive(event.timestamp);
            else
                this.onRightTurnActive(event.timestamp);
        }else if (forwardForce > this.accThresh) {
            this.onAccelerationActive(event.timestamp);
        }else if (forwardForce < -1 * this.breakThresh) {
            this.onBrakeActive(event.timestamp);
        }

        if(false) {
            Log.d(TAG, "X: " + String.valueOf(event.values[0])
                    + "\tY: " + String.valueOf(event.values[1])
                    + "\tZ: " + String.valueOf(event.values[2]));
        }
    }

    private void onAccelerationActive(long timestamp) {
        if(this.accTime == 0){
            this.accTime = timestamp;
        }else if(timestamp - this.accTime > 1000000
                && timestamp - this.lastEventTime > 5000000) {
            this.accTime = 0;
            this.lastEventTime = timestamp;
            for(AccSensorEventListener listener : this.accSensorEventListeners)
                listener.onAccelerationEvent(EVENT_ACCELERATION, timestamp);
        }
    }

    private void onBrakeActive(long timestamp) {
        if(this.breakTime == 0){
            this.breakTime = timestamp;
        }else if(timestamp - this.breakTime > 1000000
                && timestamp - this.lastEventTime > 5000000) {
            this.breakTime = 0;
            this.lastEventTime = timestamp;
            for(AccSensorEventListener listener : this.accSensorEventListeners)
                listener.onAccelerationEvent(EVENT_BREAK, timestamp);
        }
    }

    private void onLeftTurnActive(long timestamp) {
        if(this.turnTime == 0){
            this.turnTime = timestamp;
        }else if(timestamp - this.turnTime > 1000000
                && timestamp - this.lastEventTime > 5000000) {
            this.turnTime = 0;
            this.lastEventTime = timestamp;
            for (AccSensorEventListener listener : this.accSensorEventListeners)
                listener.onAccelerationEvent(EVENT_TURN_LEFT, timestamp);
        }
    }

    private void onRightTurnActive(long timestamp) {
        if(this.turnTime == 0){
            this.turnTime = timestamp;
        }else if(timestamp - this.turnTime > 1000000
                && timestamp - this.lastEventTime > 5000000) {
            this.turnTime = 0;
            this.lastEventTime = timestamp;
            for (AccSensorEventListener listener : this.accSensorEventListeners)
                listener.onAccelerationEvent(EVENT_TURN_RIGHT, timestamp);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public interface AccSensorEventListener {
        void onAccelerationEvent(int event, float timeStamp);
    }

    public static final int EVENT_TURN_LEFT = 0;
    public static final int EVENT_TURN_RIGHT = 1;
    public static final int EVENT_BREAK = 2;
    public static final int EVENT_ACCELERATION = 3;
}
