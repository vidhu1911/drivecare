package edu.neu.madcourse.shresthbhatnagar.drivecare.activities;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.content.Intent;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

import edu.neu.madcourse.shresthbhatnagar.drivecare.lib.TripTracker;
import edu.neu.madcourse.shresthbhatnagar.drivecare.lib.WearAccSensor;
import edu.neu.madcourse.shresthbhatnagar.hellomad.R;

public class DriveCare extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener, GoogleApiClient.ConnectionCallbacks {
    //Views
    private ActionBarDrawerToggle drawerToggle;
    private DrawerLayout drawerLayout;
    private ListView lv_leftDrawer;
    private LinearLayout layout_wear;
    private Button btn_start;
    private Button btn_cntWear;

    //Data
    private GoogleApiClient googleApiClient;
    private String[] sideBarMenuItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dc_activity_drive_care);

        this.sideBarMenuItem = getResources().getStringArray(R.array.drivecare_sideMenu);

        initViews();
        initListeners();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.dc_appname);
        setSupportActionBar(toolbar);

        askPermissions();

        checkLocationServices();

        this.googleApiClient = WearAccSensor.getGAPIClient(this, this);
        this.googleApiClient.connect();

        this.drawerToggle = new ActionBarDrawerToggle(this, this.drawerLayout, R.string.about_label, R.string.ok_label);
        this.drawerLayout.addDrawerListener(drawerToggle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        drawerToggle.syncState();
    }

    protected void askPermissions(){
        ActivityCompat.requestPermissions(this,
                new String[]{
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                }, 0);
    }


    private void initViews() {
        this.drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        this.lv_leftDrawer = (ListView) findViewById(R.id.left_drawer);
        this.lv_leftDrawer.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, this.sideBarMenuItem));
        this.layout_wear = (LinearLayout) findViewById(R.id.layout_wear);
        this.btn_start = (Button) findViewById(R.id.btn_start);
        this.btn_cntWear = (Button) findViewById(R.id.btn_cnt_wear);
    }

    private void initListeners() {
        this.btn_start.setOnClickListener(this);
        this.btn_cntWear.setOnClickListener(this);
        this.lv_leftDrawer.setOnItemClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()){
            case R.id.btn_start:
                intent = new Intent(this, StartMonitor.class);
                intent.putExtra("START", true);
                startActivity(intent);
                break;
            case R.id.btn_cnt_wear:
                if(!openApp(this, "com.google.android.wearable.app")){
                    Toast.makeText(this, "Please install the Android wear app", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item) || this.drawerToggle.onOptionsItemSelected(item);
    }

    public static boolean openApp(Context context, String packageName) {
        PackageManager manager = context.getPackageManager();
        Intent i = manager.getLaunchIntentForPackage(packageName);
        if (i == null) {
            return false;
        }
        i.addCategory(Intent.CATEGORY_LAUNCHER);
        context.startActivity(i);
        return true;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String title = this.sideBarMenuItem[position];
        if(title.equalsIgnoreCase(getResources().getStringArray(R.array.drivecare_sideMenu)[0])){
            Intent intent = new Intent(this, TripsHistory.class);
            startActivity(intent);
        }else if(title.equalsIgnoreCase(getResources().getStringArray(R.array.drivecare_sideMenu)[1])){
            AlertDialog confirmDeleteDialog = new AlertDialog.Builder(this)
                    .setTitle("Delete")
                    .setMessage("Are you sure you want to delete all your trip history?")
                    .setIcon(R.drawable.cross)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            TripTracker.deleteAll();
                            dialog.dismiss();
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).create();
            confirmDeleteDialog.show();
        }
    }

    protected void checkLocationServices(){
        LocationManager lm = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;
        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        }catch (Exception ex){}
        try{
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        }catch (Exception ex){}
        if(!gps_enabled && !network_enabled){
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setMessage("Location Services aren't enable. This is needed to the application to work correctly");
            dialog.setPositiveButton("Location Settings", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    Intent myIntent = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(myIntent);
                }
            });
            dialog.setNegativeButton("Exit", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    onBackPressed();
                }
            });
            dialog.show();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Wearable.NodeApi.getConnectedNodes(googleApiClient).setResultCallback(new ResultCallback<NodeApi.GetConnectedNodesResult>() {
            @Override
            public void onResult(@NonNull NodeApi.GetConnectedNodesResult getConnectedNodesResult) {
                if(getConnectedNodesResult.getNodes().size() == 0){
                    DriveCare.this.layout_wear.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    @Override
    public void onConnectionSuspended(int i) {

    }
}
