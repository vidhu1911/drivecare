package edu.neu.madcourse.shresthbhatnagar.com;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import edu.neu.madcourse.shresthbhatnagar.hellomad.R;
import edu.neu.madcourse.shresthbhatnagar.com.test.Acknowledgement;
import edu.neu.madcourse.shresthbhatnagar.com.test.PlayerListActivity;

public class MenuActivity extends AppCompatActivity {

    Button btn_test;
    Button btn_testAck;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.com_menu_activity);
        this.setTitle("Scroggle");

        //Init view ref
        btn_test = (Button) this.findViewById(R.id.btn_src2Test);
        btn_testAck = (Button) this.findViewById(R.id.btn_src2Ack);

        //Init view listeners
        btn_test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), PlayerListActivity.class);
                view.getContext().startActivity(intent);
            }
        });

        btn_testAck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), Acknowledgement.class);
                view.getContext().startActivity(intent);
            }
        });

    }
}
