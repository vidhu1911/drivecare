package edu.neu.madcourse.shresthbhatnagar.scroggle2.api;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.IgnoreExtraProperties;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import edu.neu.madcourse.shresthbhatnagar.com.api.Api;
import edu.neu.madcourse.shresthbhatnagar.com.api.Scroggle2MessagingService;

/**
 * Created by vidhu on 3/14/2017.
 */

public class GameApi {
    private static FirebaseDatabase db = FirebaseDatabase.getInstance();
    private static DatabaseReference gameReference = db.getReference("game");
    private static DatabaseReference gameIdReference;
    private static ValueEventListener gameStateChangedListener;

    private static Game lastSavedGameState;
    /**
     * Start a game between the current user and the given opponent
     *
     * @param op
     */
    public static String createGame(UserApi.User op) {
        Game newGame = Game.createGameWithOp(op);

        DatabaseReference gameRef = gameReference.push();
        String gameId = gameRef.getKey();
        gameRef.setValue(newGame);

        return gameId;
    }

    public static void updateGame(String gameId, Game game) {
        //Update game state
        gameReference.child(gameId).setValue(game);

        //Send Notification of game state change
        if(game != null && !game.equals(lastSavedGameState)) {
            Map<String, String> notification = new HashMap<>();
            notification.put("body", "A move was made in your game");
            Src2MessageingService.sendNotification(game.player1Id, notification);
            Src2MessageingService.sendNotification(game.player2Id, notification);
        }

        lastSavedGameState = game;
    }

    public static void endGame(String gameId, Game game) {
        //Update leaderboard
        Api.getInstance().getLeaderBoardApi().publishScore(game.getWinner(), game.getHighestScore());

        //Set game ended
        game.isEnd = true;
        updateGame(gameId, game);

        //Delete game
        updateGame(gameId, null);

        //Send Notification of winner
        Map<String, String> notification = new HashMap<>();
        notification.put("body", game.getWinner() + " won the game with score of " + game.getHighestScore());
        Src2MessageingService.sendNotification(game.player1Id, notification);
        Src2MessageingService.sendNotification(game.player2Id, notification);
    }

    /**
     * Used to get a list of games
     *
     * @param eventListener
     */
    public static void setGamesListener(ValueEventListener eventListener) {
        gameReference.addValueEventListener(eventListener);
    }

    public static void removeGamesListener(ValueEventListener eventListener){
        gameReference.removeEventListener(eventListener);
    }

    public static void setGameStateChangedListener(final GameStateChangedListener listener) {
        //Remove previous listeners
        removeGameStateChangedListener();

        //Create new listener with given class
        gameStateChangedListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot gameSnap : dataSnapshot.getChildren()) {
                    Game game = gameSnap.getValue(Game.class);
                    listener.gameStateChanged(game);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        //Add new listener
        String gameId = listener.getGameId();
        gameIdReference = db.getReference("game").orderByKey().equalTo(gameId).getRef();
        gameIdReference.addValueEventListener(gameStateChangedListener);
    }

    public static void removeGameStateChangedListener() {
        if (gameStateChangedListener != null)
            gameIdReference.removeEventListener(gameStateChangedListener);
    }

    public interface GameStateChangedListener {
        String getGameId();

        void gameStateChanged(Game game);
    }

    @IgnoreExtraProperties
    public static class Game {
        public String player1Name;
        public String player2Name;
        public String player1Id;
        public String player2Id;
        public int player1Score = 0;
        public int player2Score = 0;
        public boolean isPlayer1Turn;
        public String board;
        public boolean isEnd = false;

        public static Game createGameWithOp(UserApi.User op) {
            Game game = new Game();
            game.player1Name = UserApi.getUsername();
            game.player2Name = op.username;
            game.player1Id = UserApi.getCurrentUserId();
            game.player2Id = op.id;
            game.isPlayer1Turn = true;
            game.board = generateBoard();
            return game;
        }

        public String getWinner() {
            return this.player1Score < this.player2Score ? this.player2Name : this.player1Name;
        }

        public int getHighestScore() {
            return this.player1Score < this.player2Score ? this.player2Score : this.player1Score;
        }

        public boolean equals(Game game){
            return game != null &&
                    this.player1Name.equals(game.player1Name) &&
                    this.player1Id.equals(game.player1Id) &&
                    this.player1Score == game.player1Score &&
                    this.player2Name.equals(game.player2Name) &&
                    this.player2Id.equals(game.player2Id) &&
                    this.player2Score == game.player2Score &&
                    this.isPlayer1Turn == game.isPlayer1Turn &&
                    this.board.equals(game.board) &&
                    this.isEnd == game.isEnd;
        }

        private static String generateBoard() {
            final String chars = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABBBBBBBBBBBBBBCCCCCCCCCCCCCCCCCCCCCCCCCCCDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEFFFFFFFFFFFFFFFFFFFFFFGGGGGGGGGGGGGGGGGGGGHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIJKKKKKKKKLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLMMMMMMMMMMMMMMMMMMMMMMMMNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOPPPPPPPPPPPPPPPPPPPQRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTUUUUUUUUUUUUUUUUUUUUUUUUUUUUVWWWWWWWWWWWWWWWWWWWWWWWXYYYYYYYYYYYYYYYYYYYYZ";
            final int N = chars.length();
            Random r = new Random();

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < 100; i++) {
                /**
                if (i == 10) {
                    sb.append("B");
                } else if (i == 11) {
                    sb.append("A");
                } else if (i == 12) {
                    sb.append("T");
                } else if (i < 2 || i >= 6) {
                    String letter = String.valueOf(chars.charAt(r.nextInt(N)));
                    sb.append(letter);
                }
                sb.append(",");
                 **/
                String letter = String.valueOf(chars.charAt(r.nextInt(N)));
                sb.append(letter);
                sb.append(",");
            }

            return sb.toString();
        }


    }
}
