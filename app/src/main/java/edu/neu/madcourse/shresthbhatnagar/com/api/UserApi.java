package edu.neu.madcourse.shresthbhatnagar.com.api;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.IgnoreExtraProperties;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

/**
 * Created by vidhu on 2/28/2017.
 */

public class UserApi {
    public static String STATUS_OFFLINE = "0";
    public static String STATUS_ONLINE = "1";
    public static String STATUS_INGAME = "2";

    private FirebaseDatabase db = FirebaseDatabase.getInstance();

    private String username;

    public void setAvailableUsersListener(ValueEventListener eventListener){
        DatabaseReference users = db.getReference("users");
        users.orderByChild("status").equalTo(UserApi.STATUS_ONLINE)
                .addValueEventListener(eventListener);
    }


    public void saveUser(User user) {
        this.username = user.username;
        final DatabaseReference users = db.getReference("users");
        if(user.status == STATUS_OFFLINE) {
            users.orderByChild("id").equalTo(user.id).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot user : dataSnapshot.getChildren()) {
                        users.child(user.getKey()).setValue(null);
                    }
                }
                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }else
            users.push().setValue(user);
    }

    public String getCurrentUserId() {
        return FirebaseInstanceId.getInstance().getToken();
    }

    public String getCurrentUsername() {
        return this.username;
    }

    @IgnoreExtraProperties
    public static class User {
        public String id;
        public String username;
        public String status;

        public User(){

        }

        public User(String username, String status, String id){
            this.username = username;
            this.status = status;
            this.id = id;
        }

        public User(String username, String status){
            this(username, status, FirebaseInstanceId.getInstance().getToken());
        }

        @Override
        public String toString() {
            return username;
        }
    }
}
