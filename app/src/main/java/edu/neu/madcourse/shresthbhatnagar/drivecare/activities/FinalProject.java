package edu.neu.madcourse.shresthbhatnagar.drivecare.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.content.Intent;

import edu.neu.madcourse.shresthbhatnagar.hellomad.R;

public class FinalProject extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dc_activity_final_project);


        findViewById(R.id.btn_runApp).setOnClickListener(this);
        findViewById(R.id.btn_runAck).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.btn_runApp:
                intent = new Intent(this, DriveCare.class);
                startActivity(intent);
                break;
            case R.id.btn_runAck:
                intent = new Intent(this, Acknowledgement.class);
                startActivity(intent);
                break;
        }
    }
}
