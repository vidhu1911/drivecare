package edu.neu.madcourse.shresthbhatnagar.com.test;

import android.app.NotificationManager;
import android.content.Context;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.Map;

import edu.neu.madcourse.shresthbhatnagar.hellomad.R;
import edu.neu.madcourse.shresthbhatnagar.com.api.Api;
import edu.neu.madcourse.shresthbhatnagar.com.api.GameApi;
import edu.neu.madcourse.shresthbhatnagar.com.api.Scroggle2MessagingService;

public class GamePlay extends AppCompatActivity implements Button.OnClickListener, Scroggle2MessagingService.OnMessageReceiveListener{

    String gameId;
    GameApi.Game game;
    boolean isPlayer1;

    TextView tv_gameId;
    TextView tv_player1Name;
    TextView tv_player2Name;
    TextView tv_player1Score;
    TextView tv_player2Score;
    TextView tv_turn;
    Button btn_addScore50;
    Button btn_addScore100;
    Button btn_endgame;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_activity_game_play);

        //Init intent data
        this.gameId = this.getIntent().getStringExtra(GameApi.PROP_GAMEID);

        //Init view references
        this.tv_gameId = (TextView) this.findViewById(R.id.tv_test_gameid);
        this.tv_player1Score = (TextView) this.findViewById(R.id.tv_test_player1Score);
        this.tv_player2Score = (TextView) this.findViewById(R.id.tv_test_player2Score);
        this.tv_player1Name = (TextView) this.findViewById(R.id.tv_test_player1Name);
        this.tv_player2Name = (TextView) this.findViewById(R.id.tv_test_player2Name);
        this.tv_turn = (TextView) this.findViewById(R.id.tv_test_turn);
        this.btn_addScore50 = (Button) this.findViewById(R.id.btn_test_move50);
        this.btn_addScore100 = (Button) this.findViewById(R.id.btn_test_move100);
        this.btn_endgame = (Button) this.findViewById(R.id.btn_test_endgame);

        //Init view data
        this.tv_gameId.setText(this.gameId);

        //Init listeners
        this.btn_addScore50.setOnClickListener(this);
        this.btn_addScore100.setOnClickListener(this);
        this.btn_endgame.setOnClickListener(this);

        Scroggle2MessagingService.addOnMessageReceiveListener(this);

        Api.getInstance().getGameApi().setOnGameUpdateListener(this.gameId, new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot gameSnap : dataSnapshot.getChildren()){
                    game = gameSnap.getValue(GameApi.Game.class);
                    updateGameState();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id){
            case R.id.btn_test_endgame:
                endGame();
                break;
            case R.id.btn_test_move50:
                this.addScore(50);
                break;
            case R.id.btn_test_move100:
                this.addScore(100);
                break;
        }
    }

    protected void updateGameState(){
        if(this.game.player1Id.equals(Api.getInstance().getUserApi().getCurrentUserId()))
            this.isPlayer1 = true;
        else
            this.isPlayer1 = false;
        if((this.game.isPlayer1Turn && this.isPlayer1) || (!this.game.isPlayer1Turn && !this.isPlayer1)) {
            this.tv_turn.setText("Your turn");
            this.btn_addScore50.setEnabled(true);
            this.btn_addScore100.setEnabled(true);
        } else {
            this.tv_turn.setText("Opponent's turn");
            this.btn_addScore50.setEnabled(false);
            this.btn_addScore100.setEnabled(false);
        }

        this.tv_player1Score.setText(String.valueOf(this.game.player1Score));
        this.tv_player2Score.setText(String.valueOf(this.game.player2Score));
        this.tv_player1Name.setText(this.game.player1Name);
        this.tv_player2Name.setText(this.game.player2Name);
    }

    protected void endGame(){
        Api.getInstance().getGameApi().endGame(this.gameId, this.game);
    }

    protected void addScore(int score){
        //Update Score
        if(this.isPlayer1)
            this.game.player1Score += score;
        else
            this.game.player2Score += score;

        //Update player turn
        this.game.isPlayer1Turn = !this.game.isPlayer1Turn;

        Api.getInstance().getGameApi().updateGame(this.gameId, this.game);
    }

    protected void showWinnerNotification(){
        String winner = game.player1Score < game.player2Score ? game.player2Name : game.player1Name;
        int score = game.player1Score < game.player2Score ? game.player2Score : game.player1Score;

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder nb = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Scroggle2")
                .setContentText(winner + " won with score of " + score)
                .setSound(defaultSoundUri);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 , nb.build());
    }

    @Override
    public void onMessage(Map<String, String> data) {
        String action = data.get(GameApi.EVENT);
        if(action.equals(GameApi.EVENT_END)){
            showWinnerNotification();
            finish();
        }
    }
}
