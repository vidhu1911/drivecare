package edu.neu.madcourse.shresthbhatnagar.scroggle2.game;

import android.content.Context;
import android.content.res.AssetManager;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;

/**
 * Created by vidhu on 3/14/2017.
 */

public class WordChecker {
    private AssetManager assetManager;
    private AsyncTask<BufferedReader, String, Void> wordLoader;
    private HashSet<String> words = new HashSet<>();

    public WordChecker(AssetManager assetManager) {
        this.assetManager = assetManager;
    }

    /**
     * Loads words from dictionary starting with the given letter
     * @param first
     */
    public void loadWords(String first) {
        first = first.toLowerCase();
        InputStream is = null;
        try {
            is = this.assetManager.open("dict_words/dict." + first + ".txt");
        } catch (Exception ex) {
            ex.printStackTrace();
            return;
        }
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        this.wordLoader = new WordLoader();
        this.wordLoader.execute(br);
    }

    /**
     * Check if word is in dictionary
     *
     * @param word
     * @return True if given word is in the dictionary
     */
    public boolean isWord(String word) {
        word = word.toLowerCase();
        return this.words.contains(word);
    }

    private class WordLoader extends AsyncTask<BufferedReader, String, Void> {

        @Override
        protected Void doInBackground(BufferedReader... reader) {
            try {
                String line;
                while ((line = reader[0].readLine()) != null) {
                    words.add(line);
                }
            } catch (Exception ex) {
                Log.e("Error:", ex.getMessage());
            }

            return null;
        }

    }
}
