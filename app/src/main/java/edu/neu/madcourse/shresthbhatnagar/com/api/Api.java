package edu.neu.madcourse.shresthbhatnagar.com.api;

/**
 * Created by vidhu on 2/28/2017.
 */

public class Api {
    private UserApi userApi;
    private GameApi gameApi;
    private LeaderBoardApi leaderBoardApi;

    public Api(){
        this.userApi = new UserApi();
        this.gameApi = new GameApi();
        this.leaderBoardApi = new LeaderBoardApi();
    }

    public UserApi getUserApi() {
        return userApi;
    }

    public GameApi getGameApi() {
        return gameApi;
    }

    public LeaderBoardApi getLeaderBoardApi() {
        return leaderBoardApi;
    }

    private static Api instance = null;
    public static Api getInstance() {
        if(instance == null){
            instance = new Api();
        }
        return instance;
    }
}
