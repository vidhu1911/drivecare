package edu.neu.madcourse.shresthbhatnagar.drivecare.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.List;

import edu.neu.madcourse.shresthbhatnagar.drivecare.lib.AccSensor;
import edu.neu.madcourse.shresthbhatnagar.drivecare.lib.TripTracker;
import edu.neu.madcourse.shresthbhatnagar.hellomad.R;

/**
 * Created by vidhu on 4/21/2017.
 */

public class EventListAdapter extends BaseAdapter {

    private final Context context;
    private final List<TripTracker.TripPoint> tripPoints;
    private static LayoutInflater inflater;
    private static SimpleDateFormat time = new SimpleDateFormat("h:mm a");

    public EventListAdapter(Context context, List<TripTracker.TripPoint> tripPoints){
        this.context = context;
        this.tripPoints = tripPoints;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return this.tripPoints.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        if(convertView == null)
            vi = inflater.inflate(R.layout.dc_event_list_item, null);

        ImageView iv_icon = (ImageView) vi.findViewById(R.id.iv_eventIcon);
        TextView tv_event = (TextView) vi.findViewById(R.id.tv_eventName);
        TextView tv_time = (TextView) vi.findViewById(R.id.tv_eventTime);

        TripTracker.TripPoint tripPoint = this.tripPoints.get(position);

        Bitmap icon = null;
        switch (tripPoint.getEvent()){
            case AccSensor.EVENT_ACCELERATION:
                icon = BitmapFactory.decodeResource(context.getResources(), R.drawable.dc_event_acc);
                tv_event.setText("Hard Acceleration");
                break;
            case AccSensor.EVENT_BREAK:
                icon = BitmapFactory.decodeResource(context.getResources(), R.drawable.dc_event_brake);
                tv_event.setText("Hard Braking");
                break;
            case AccSensor.EVENT_TURN_LEFT:
                icon = BitmapFactory.decodeResource(context.getResources(), R.drawable.dc_event_left);
                tv_event.setText("Hard Left");
                break;
            case AccSensor.EVENT_TURN_RIGHT:
                icon = BitmapFactory.decodeResource(context.getResources(), R.drawable.dc_event_right);
                tv_event.setText("Hard Right");
                break;
        }
        iv_icon.setImageBitmap(icon);
        tv_time.setText(time.format(tripPoint.getTime()));

        return vi;
    }
}
