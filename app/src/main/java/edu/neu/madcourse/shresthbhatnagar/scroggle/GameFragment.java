package edu.neu.madcourse.shresthbhatnagar.scroggle;

import android.content.res.AssetManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import edu.neu.madcourse.shresthbhatnagar.hellomad.R;

public class GameFragment extends Fragment {
    static private int mLargeIds[] = {R.id.scrLarge1, R.id.scrLarge2, R.id.scrLarge3,
            R.id.scrLarge4, R.id.scrLarge5, R.id.scrLarge6,
            R.id.scrLarge7, R.id.scrLarge8, R.id.scrLarge9};

    static private int mSmallIds[] = {R.id.scrSmall1, R.id.scrSmall2, R.id.scrSmall3,
            R.id.scrSmall4, R.id.scrSmall5, R.id.scrSmall6,
            R.id.scrSmall7, R.id.scrSmall8, R.id.scrSmall9};
    private TileGame mEntireBoard;
    private TileGame mLargeTiles[] = new TileGame[9];
    private TileLetter mSmallTiles[][] = new TileLetter[9][9];
    private int mLastLarge;
    private int mLastSmall;

    private String currentWord = "";
    private int score = 0;


    private String[] words = new String[9];

    private static int[][] neighbors = {
            {1, 3, 4},
            {0, 3, 4, 5, 2},
            {1, 4, 5},
            {0, 1, 4, 7, 6},
            {0, 1, 2, 3, 5, 6, 7, 8},
            {1, 2, 4, 7, 8},
            {3, 4, 7},
            {6, 3, 4, 5, 8},
            {7, 4, 5}
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setRetainInstance(true);
        initWordList();
        initGame();
    }

    private void initWordList() {
        AssetManager am = getContext().getAssets();
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(am.open("common9words.txt")));
            List<String> tmpWords = new ArrayList<>();
            String line = reader.readLine();
            while (line != null){
                tmpWords.add(line);
                line = reader.readLine();
            }
            reader.close();
            Random r = new Random();
            for(int i=0; i<9; i++){
                this.words[i] = tmpWords.get(r.nextInt(tmpWords.size()));
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.scroggle_large_board, container, false);
        initViews(rootView);
        updateAllTiles();
        return rootView;
    }

    protected void initGame() {
        this.mEntireBoard = new TileGame(this, TileGame.Type.FULL);
        //Create all the tiles
        for (int large = 0; large < 9; large++) {
            this.mLargeTiles[large] = new TileGame(this, TileGame.Type.PART);
            this.mLargeTiles[large].setWord(this.words[large]);

            for (int small = 0; small < 9; small++) {
                this.mSmallTiles[large][small] = new TileLetter(this);
            }

            this.mLargeTiles[large].setSubTiles(this.mSmallTiles[large]);
        }
        //this.mEntireBoard.setSubTiles(this.mLargeTiles);
    }

    protected void initViews(View rootView) {
        this.mEntireBoard.setView(rootView);

        for (int large = 0; large < 9; large++) {
            View outer = rootView.findViewById(mLargeIds[large]);
            this.mLargeTiles[large].setView(outer);

            for (int small = 0; small < 9; small++) {
                LetterTileView inner = (LetterTileView) outer.findViewById(mSmallIds[small]);
                final int fLarge = large;
                final int fSmall = small;
                final TileLetter smallTile = this.mSmallTiles[large][small];
                smallTile.setView(inner);
                inner.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        makeMove(fLarge, fSmall);
                    }
                });
            }

            this.mLargeTiles[large].initTiles();
        }

    }

    protected void updateAllTiles() {
        this.mEntireBoard.updateDrawableState();
        for (int large = 0; large < 9; large++) {
            mLargeTiles[large].updateDrawableState();
            for (int small = 0; small < 9; small++) {
                mSmallTiles[large][small].updateDrawableState();
            }
        }
    }

    protected void makeMove(int large, int small) {
        Log.d("UPDATE", large + " Tile");

        if (!this.mSmallTiles[large][small].isActive())
            return;

        if (!this.mLargeTiles[large].isPlayable())
            return;

        this.mLastLarge = large;
        this.mLastSmall = small;
        this.currentWord += this.mSmallTiles[large][small].getLetter();
        setActive(large, small);
        updateAllTiles();
        updateCurrentWord();
    }


    protected void setActive(int large, int small) {
        this.deactivateAll();

        this.mSmallTiles[large][small].setChoosen(true);
        for (int i : neighbors[small]) {
            TileLetter tileLetter = this.mSmallTiles[large][i];
            if (!tileLetter.isChoosen())
                tileLetter.setActive(true);
        }
    }

    protected void deactivateAll() {
        for (int large = 0; large < 9; large++) {
            for (int small = 0; small < 9; small++) {
                this.mSmallTiles[large][small].setActive(false);
            }
        }
    }

    protected void unselectAll() {
        this.currentWord = "";
        this.updateCurrentWord();
        for (int large = 0; large < 9; large++) {
            if (!this.mLargeTiles[large].isPlayable())
                continue;
            for (int small = 0; small < 9; small++) {
                this.mSmallTiles[large][small].setChoosen(false);
                this.mSmallTiles[large][small].setActive(true);
            }
        }
        updateAllTiles();
    }

    protected void updateCurrentWord() {
        ((GameActivity) getActivity()).updateCurrentWord(this.currentWord);
    }

    public void finishSmall() {
        for (int small = 0; small < 9; small++) {
            TileLetter tileLetter = this.mSmallTiles[this.mLastLarge][small];
            if (!tileLetter.isChoosen())
                tileLetter.setLetter("");
            tileLetter.setActive(false);
        }
        this.mLargeTiles[this.mLastLarge].setPlayable(false);
        this.unselectAll();
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getCurrentWord() {
        return currentWord;
    }

    public void setCurrentWord(String currentWord) {
        this.currentWord = currentWord;
    }

    public String[] getRemainingLetters(){
        List<String> letters = new ArrayList<>();
        for(int large=0; large<9; large++){
            for(int small=0; small<9; small++){
                String letter = this.mSmallTiles[large][small].getLetter();
                if(this.mSmallTiles[large][small].isChoosen() || letter.equals(""))
                    continue;
                letters.add(letter);
            }
        }
        return letters.toArray(new String[0]);
    }

    public String getState() {
        StringBuilder builder = new StringBuilder();

        builder.append(this.currentWord);
        builder.append(",");
        builder.append(this.score);
        builder.append(",");

        for (int large = 0; large < 9; large++) {
            int isPlayable = this.mLargeTiles[large].isPlayable() ? 1 : 0;
            String word = this.mLargeTiles[large].getWord();
            int pathIdx = this.mLargeTiles[large].getPathIdx();

            builder.append(isPlayable);
            builder.append(",");
            builder.append(word);
            builder.append(",");
            builder.append(pathIdx);
            builder.append(",");

            for (int small = 0; small < 9; small++) {
                int isActive = this.mSmallTiles[large][small].isActive() ? 1 : 0;
                int isChoosen = this.mSmallTiles[large][small].isChoosen() ? 1 : 0;
                String character = this.mSmallTiles[large][small].getLetter();
                builder.append(isActive);
                builder.append(",");
                builder.append(isChoosen);
                builder.append(",");
                builder.append(character);
                builder.append(",");
            }
        }

        return builder.toString();
    }

    public void putState(String gameData) {
        String[] fields = gameData.split(",");

        int idx = 0;

        this.currentWord = fields[idx++];
        this.score = Integer.valueOf(fields[idx++]);

        for(int large=0; large<9; large++){
            boolean isPlayable = fields[idx++].equals("1");
            String word = fields[idx++];
            int pathIdx = Integer.valueOf(fields[idx++]);

            this.mLargeTiles[large].setPlayable(isPlayable);
            this.mLargeTiles[large].setWord(word);
            this.mLargeTiles[large].setPathIdx(pathIdx);

            for(int small = 0; small < 9; small++) {
                boolean isActive = fields[idx++].equals("1");
                boolean isChoosen = fields[idx++].equals("1");
                String character = fields[idx++];
                this.mSmallTiles[large][small].setActive(isActive);
                this.mSmallTiles[large][small].setChoosen(isChoosen);
                this.mSmallTiles[large][small].setLetter(character);
            }
        }

        this.updateAllTiles();
    }
}
