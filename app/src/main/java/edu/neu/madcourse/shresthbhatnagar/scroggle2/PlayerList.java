package edu.neu.madcourse.shresthbhatnagar.scroggle2;

import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import edu.neu.madcourse.shresthbhatnagar.com.test.Leaderboard;
import edu.neu.madcourse.shresthbhatnagar.hellomad.R;
import edu.neu.madcourse.shresthbhatnagar.scroggle2.api.GameApi;
import edu.neu.madcourse.shresthbhatnagar.scroggle2.api.NetworkStateReceiver;
import edu.neu.madcourse.shresthbhatnagar.scroggle2.api.UserApi;
import edu.neu.madcourse.shresthbhatnagar.scroggle2.game.GameActivity;

public class PlayerList extends AppCompatActivity implements NetworkStateReceiver.NetworkStateReceiverListener{

    Button btn_ack;
    Button btn_leaderboard;
    EditText et_username;
    SwitchCompat sw_isAvailable;
    ListView lv_availableUsers;

    ArrayList<UserApi.User> availableUsers;
    ArrayAdapter<UserApi.User> availableUsersAdapter;

    ValueEventListener gamesListener;
    NetworkStateReceiver networkStateReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scr2_activity_player_list);
        this.setTitle("Two Player Word Game");

        this.networkStateReceiver = new NetworkStateReceiver();
        this.networkStateReceiver.addListener(this);

        //Init data objects
        availableUsers = new ArrayList<>();
        availableUsersAdapter = new ArrayAdapter<UserApi.User>(this, android.R.layout.simple_list_item_1, availableUsers);

        //Init other stuff
        initViews();
        initViewListeners();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = this.getMenuInflater();
        inflater.inflate(R.menu.scr2_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_help:
                Intent intent = new Intent(this, HowToPlay.class);
                this.startActivity(intent);
                break;
        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        initDataListeners();

        //network listener
        this.registerReceiver(networkStateReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    @Override
    protected void onPause() {
        super.onPause();
        this.sw_isAvailable.setChecked(false);
        this.setUserOffline();
        GameApi.removeGamesListener(this.gamesListener);

        //network listener
        this.unregisterReceiver(networkStateReceiver);
    }

    private void initViews() {
        this.btn_ack = (Button) findViewById(R.id.btn_src2_ack);
        this.btn_leaderboard = (Button) findViewById(R.id.btn_src2_leaderboard);
        this.et_username = (EditText) findViewById(R.id.et_src2_username);
        this.sw_isAvailable = (SwitchCompat) findViewById(R.id.switch_src2_available);
        this.lv_availableUsers = (ListView) findViewById(R.id.lv_scr2_availableUsers);

        this.et_username.setText(this.getSavedUsername());
        this.lv_availableUsers.setAdapter(this.availableUsersAdapter);
    }

    private void initViewListeners() {
        //Acknowledgement button
        this.btn_ack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), Acknowledgement.class);
                startActivity(intent);
            }
        });

        //Leaderboard button
        this.btn_leaderboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), Leaderboard.class);
                startActivity(intent);
            }
        });

        //Available switch toggle button
        this.sw_isAvailable.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    if (et_username.getText().length() == 0) {
                        Toast.makeText(compoundButton.getContext(), "Enter a username", Toast.LENGTH_SHORT)
                                .show();
                        sw_isAvailable.setChecked(false);
                    } else {
                        setUserAvailable();
                        saveUsername(et_username.getText().toString());
                    }
                } else {
                    setUserOffline();
                }
            }
        });

        //Username click
        this.lv_availableUsers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                UserApi.User user = availableUsers.get(position);
                if(user.id.equals(UserApi.getCurrentUserId())) {
                    Toast.makeText(view.getContext(), "Can't start a game with yourself", Toast.LENGTH_SHORT)
                            .show();
                }else{
                    GameApi.createGame(user);
                    //Game will start automatically when listener listens to the newly added game
                }
            }
        });
    }

    private void initDataListeners() {

        UserApi.setAvailableUsersListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                availableUsers.clear();
                for(DataSnapshot user : dataSnapshot.getChildren()) {
                    UserApi.User u = user.getValue(UserApi.User.class);
                    availableUsers.add(u);
                }
                availableUsersAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });

        this.gamesListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final String userId = UserApi.getCurrentUserId();
                for(DataSnapshot gameSnap : dataSnapshot.getChildren()){
                    GameApi.Game game = gameSnap.getValue(GameApi.Game.class);
                    if(game.player1Id.equals(userId) || game.player2Id.equals(userId))
                        if(!game.isEnd)
                            startGame(gameSnap.getKey());
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        GameApi.setGamesListener(gamesListener);
    }

    private void setUserAvailable() {
        String username = et_username.getText().toString();
        UserApi.addAvailableUser(username);
    }

    private void setUserOffline() {
        UserApi.removeAvailableUser();
    }

    protected String getSavedUsername(){
        return this.getPreferences(MODE_PRIVATE).getString("username", "");
    }

    protected void saveUsername(String username){
        this.getPreferences(MODE_PRIVATE).edit()
                .putString("username", username)
                .commit();
    }


    public void startGame(String gameId) {
        Toast.makeText(this, "Game Started!", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, GameActivity.class);
        intent.putExtra("GAMEID", gameId);
        startActivity(intent);
    }

    @Override
    public void networkAvailable() {
        //Toast.makeText(this, "Connected to the Internet!!", Toast.LENGTH_SHORT).show();
        this.sw_isAvailable.setEnabled(true);
        this.btn_leaderboard.setEnabled(true);
    }

    @Override
    public void networkUnavailable() {
        Toast.makeText(this, "No internet connection!", Toast.LENGTH_SHORT).show();
        this.sw_isAvailable.setEnabled(false);
        this.btn_leaderboard.setEnabled(false);
    }
}
