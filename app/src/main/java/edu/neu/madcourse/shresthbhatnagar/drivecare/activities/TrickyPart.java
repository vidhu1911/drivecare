package edu.neu.madcourse.shresthbhatnagar.drivecare.activities;

import android.Manifest;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.content.Intent;

import edu.neu.madcourse.shresthbhatnagar.drivecare.lib.AccSensor;
import edu.neu.madcourse.shresthbhatnagar.drivecare.lib.LocationSensor;
import edu.neu.madcourse.shresthbhatnagar.drivecare.lib.TripTracker;
import edu.neu.madcourse.shresthbhatnagar.drivecare.lib.WearAccSensor;
import edu.neu.madcourse.shresthbhatnagar.hellomad.R;

import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.DataEventBuffer;

public class TrickyPart extends AppCompatActivity implements
        AccSensor.AccSensorEventListener,
        LocationListener,
        OnMapReadyCallback,
        DataApi.DataListener,
        WearAccSensor.OnWearMesurement{
    private static final String TAG = TrickyPart.class.getSimpleName();

    //Views
    GoogleMap liveMap;
    private TextView tv_status;
    private Switch swch_toggleDriving;
    private TextView tv_locLat;
    private TextView tv_locLng;
    private TextView tv_accx;
    private TextView tv_accy;
    private TextView tv_accz;

    //Sensors
    private AccSensor accSensor;
    private LocationSensor locationSensor;
    private WearAccSensor wearAccSensor;

    //Data
    private Marker carMarker;
    private TripTracker tripTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dc_activity_tricky_part);

        askPermissions();
        initViews();
        initListeners();

        this.locationSensor = new LocationSensor(this, this);
        this.accSensor = new AccSensor(this, 5.0f, 5.0f, 5.0f);
        this.accSensor.addListener(this);

        this.wearAccSensor = new WearAccSensor(this);
        this.wearAccSensor.registerListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.locationSensor.start();
        this.wearAccSensor.start();
    }

    @Override
    protected void onStop() {
        super.onStop();
        this.locationSensor.stop();
        this.accSensor.stopSensor();
        this.wearAccSensor.stop();
    }

    protected void initViews() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.livemap);
        mapFragment.getMapAsync(this);

        this.tv_status = (TextView) findViewById(R.id.tv_drivingStatus);
        this.swch_toggleDriving = (Switch) findViewById(R.id.swch_toggleDriving);
        this.tv_locLat = (TextView) findViewById(R.id.tv_loclat);
        this.tv_locLng = (TextView) findViewById(R.id.tv_loclng);
        this.tv_accx = (TextView) findViewById(R.id.tv_accx);
        this.tv_accy = (TextView) findViewById(R.id.tv_accy);
        this.tv_accz = (TextView) findViewById(R.id.tv_accz);
    }

    protected void initListeners() {
        this.swch_toggleDriving.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    startMonitoringDriving();
                } else {
                    stopMonitoringDriving();
                }
            }
        });
    }

    public void showNotification() {
        Intent viewIntent = new Intent(this, TrickyPart.class);
        PendingIntent viewPendingIntent = PendingIntent.getActivity(this, 0, viewIntent, 0);

        NotificationCompat.Builder nb = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Driving Alert")
                .setContentText("Hard Acceleration detected")
                .setContentIntent(viewPendingIntent)
                .setDefaults(Notification.DEFAULT_ALL)
                .addAction(R.drawable.check, "OK", viewPendingIntent)
                .addAction(R.drawable.cross, "Dismiss", viewPendingIntent);

        NotificationManagerCompat nm = NotificationManagerCompat.from(this);
        nm.notify(001, nb.build());
    }

    protected void startMonitoringDriving() {
        this.accSensor.startSensor();
        this.tripTracker = new TripTracker(this);
        this.tv_status.setText("Driving is being now monitored");
        Toast.makeText(this, "Sensors started", Toast.LENGTH_LONG).show();
    }

    protected void stopMonitoringDriving() {
        accSensor.stopSensor();
        this.tripTracker.saveToSDCard();
        this.tv_status.setText("Driving monitor is stopped");
        Toast.makeText(this, "Sensors stopped", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onAccelerationEvent(int event, float timeStamp) {
        switch (event) {
            case AccSensor.EVENT_ACCELERATION:
                Log.d(TAG, "Acceleration Event");
                break;
            case AccSensor.EVENT_BREAK:
                Log.d(TAG, "Break Event");
                break;
            case AccSensor.EVENT_TURN_LEFT:
            case AccSensor.EVENT_TURN_RIGHT:
                Log.d(TAG, "Turn event");
                break;
        }
        this.tripTracker.addPoint(event);
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "Location: " + location.getLatitude() + "," + location.getLongitude());
        this.tv_locLat.setText("Latitude:\t" + location.getLatitude());
        this.tv_locLng.setText("Longitude:\t" + location.getLongitude());
        this.updateMap(location);
        if(this.tripTracker != null)
            this.tripTracker.addPoint(location);
    }

    public void updateMap(Location location){
        if(this.liveMap == null)
            return;

        //Set Camera
        CameraUpdate center = CameraUpdateFactory.newLatLng(new LatLng(
                location.getLatitude(),
                location.getLongitude()));
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(15);
        this.liveMap.moveCamera(center);
        this.liveMap.moveCamera(zoom);

        //Set Markers
        if(this.carMarker != null)
            this.carMarker.remove();
        MarkerOptions markerOptions = new MarkerOptions()
                .position(new LatLng(
                        location.getLatitude(),
                        location.getLongitude()))
                .icon(BitmapDescriptorFactory.fromBitmap(this.getCarIcon(location.getBearing())));
        this.carMarker = this.liveMap.addMarker(markerOptions);
    }

    public Bitmap getCarIcon(float bearing){
        Matrix matrix = new Matrix();
        matrix.postRotate(bearing);

        Bitmap b = Bitmap.createScaledBitmap(
                ((BitmapDrawable) ResourcesCompat.getDrawable(getResources(), R.drawable.car_map_marker, null)).getBitmap(),
                50, 50, false);
        Bitmap br = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), matrix, true);
        return br;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.liveMap = googleMap;
    }

    protected void askPermissions(){
        ActivityCompat.requestPermissions(this,
                new String[]{
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
        }, 0);
    }

    @Override
    public void onDataChanged(DataEventBuffer dataEventBuffer) {

    }

    @Override
    public void OnWearMeasurement(long timestamp, float[] values) {
        //Log.d(TAG, String.valueOf(values[0]) + " " + String.valueOf(values[1]) + " " + String.valueOf(values[2]));
        this.tv_accx.setText("Acc X: " + String.valueOf(values[0]));
        this.tv_accy.setText("Acc Y: " + String.valueOf(values[1]));
        this.tv_accz.setText("Acc Z: " + String.valueOf(values[2]));
    }
}
