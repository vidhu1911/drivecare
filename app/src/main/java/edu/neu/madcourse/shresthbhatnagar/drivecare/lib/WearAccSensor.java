package edu.neu.madcourse.shresthbhatnagar.drivecare.lib;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataItem;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.Wearable;

import java.util.List;

/**
 * Created by vidhu on 4/11/2017.
 */

public class WearAccSensor implements GoogleApiClient.ConnectionCallbacks, DataApi.DataListener {
    public static final String TAG = WearAccSensor.class.getSimpleName();

    GoogleApiClient googleApiClient;
    OnWearMesurement onWearMesurementListener;

    public WearAccSensor(Context context) {
        this.googleApiClient = WearAccSensor.getGAPIClient(context, this);
    }

    public void start() {
        googleApiClient.connect();
    }

    public void stop() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                sendMeasurementCommand("stop");
                googleApiClient.disconnect();
            }
        }).start();
    }

    public void registerListener(OnWearMesurement onWearMesurementListener){
        this.onWearMesurementListener = onWearMesurementListener;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Wearable.DataApi.addListener(this.googleApiClient, this);
        new Thread(new Runnable() {
            @Override
            public void run() {
                sendMeasurementCommand("start");
            }
        }).start();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onDataChanged(DataEventBuffer dataEventBuffer) {
        for(DataEvent event : dataEventBuffer){
            Log.d(TAG, String.valueOf(event.getType()));
            if(event.getType() == DataEvent.TYPE_CHANGED) {
                DataItem item = event.getDataItem();
                if(item.getUri().getPath().compareTo("/acc") == 0) {
                    DataMap dataMap = DataMapItem.fromDataItem(item).getDataMap();
                    long time = dataMap.getLong("time");
                    float[] values = dataMap.getFloatArray("values");
                    if(this.onWearMesurementListener != null)
                        this.onWearMesurementListener.OnWearMeasurement(time, values);
                }
            }
        }
    }

    protected void sendMeasurementCommand(final String cmd){
        Log.d(TAG, "Sending Command");
        List<Node> nodes = Wearable.NodeApi.getConnectedNodes(googleApiClient).await().getNodes();
        for(Node node : nodes) {
            Wearable.MessageApi.sendMessage(this.googleApiClient, node.getId(), "/"+cmd, null)
                    .setResultCallback(new ResultCallback<MessageApi.SendMessageResult>() {
                        @Override
                        public void onResult(@NonNull MessageApi.SendMessageResult sendMessageResult) {
                            //Log.d(TAG, "controlMeasurementInBackground(/" + cmd +"): " + sendMessageResult.getStatus().isSuccess());
                        }
                    });
        }
    }

    public interface OnWearMesurement {
        void OnWearMeasurement(long timestamp, float[] values);
    }

    public static GoogleApiClient getGAPIClient(Context context, GoogleApiClient.ConnectionCallbacks callbacks){
        GoogleApiClient.Builder apiClient =  new GoogleApiClient.Builder(context)
                .addApi(Wearable.API);

        if(callbacks != null){
            apiClient.addConnectionCallbacks(callbacks);
        }

        return apiClient.build();
    }
}
