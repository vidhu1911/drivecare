package edu.neu.madcourse.shresthbhatnagar.scroggle2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

import edu.neu.madcourse.shresthbhatnagar.hellomad.R;

public class Acknowledgement extends AppCompatActivity {

    WebView wv_ack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scr2_activity_acknowledgement);

        this.wv_ack = (WebView) findViewById(R.id.wv_src_ack);
        this.wv_ack.getSettings().setJavaScriptEnabled(true);
        this.wv_ack.loadData(getString(R.string.twoPlayerWordGameAck), "text/html", null);
    }
}
