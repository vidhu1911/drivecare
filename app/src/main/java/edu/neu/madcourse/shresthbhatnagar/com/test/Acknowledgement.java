package edu.neu.madcourse.shresthbhatnagar.com.test;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

import edu.neu.madcourse.shresthbhatnagar.hellomad.R;

public class Acknowledgement extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_activity_acknowledgement);

        WebView tvScrAbout = (WebView) findViewById(R.id.wv_test_ack);
        tvScrAbout.getSettings().setJavaScriptEnabled(true);
        tvScrAbout.loadData(getString(R.string.scroggle2TestAck), "text/html", null);
    }
}
