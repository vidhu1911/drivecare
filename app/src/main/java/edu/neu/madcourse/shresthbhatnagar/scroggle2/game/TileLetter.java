package edu.neu.madcourse.shresthbhatnagar.scroggle2.game;


import android.view.View;

import edu.neu.madcourse.shresthbhatnagar.scroggle.GameFragment;

/**
 * Created by vidhu on 2/13/2017.
 */

public class TileLetter {


    private LetterTileView mView;

    private String letter = "A";
    private boolean isActive = true;
    private boolean isChoosen = false;


    public boolean isChoosen() {
        return isChoosen;
    }

    public void setChoosen(boolean choosen) {
        isChoosen = choosen;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public String getLetter() {
        return letter;
    }

    public void setLetter(String letter) {
        this.letter = letter;
        //Deactive tile it no letter in it
        if(letter.equals(""))
            this.setActive(false);
    }

    public void updateDrawableState() {
        this.mView.invalidate();
    }

    public void setView(LetterTileView view) {
        this.mView = view;
        this.mView.setTileLetter(this);
    }
}
