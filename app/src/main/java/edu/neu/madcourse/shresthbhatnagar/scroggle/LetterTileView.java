package edu.neu.madcourse.shresthbhatnagar.scroggle;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by vidhu on 2/8/2017.
 */

public class LetterTileView extends View{

    private Paint paintBg = new Paint();
    private Paint paintBgActive = new Paint();
    private Paint paintFrameOutline = new Paint();
    private Paint paintFrameOutlineSelected = new Paint();
    private Paint paintText = new Paint();

    private TileLetter tileLetter;

    public LetterTileView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        this.setLayoutParams(new ViewGroup.LayoutParams(100, 100));

        paintBg.setARGB(255, 69, 28, 91);
        paintBg.setAntiAlias(true);

        paintBgActive.setARGB(255, 100, 70, 120);
        paintBgActive.setAntiAlias(true);

        paintFrameOutline.setARGB(255, 189, 115, 228);
        paintFrameOutline.setAntiAlias(true);
        paintFrameOutline.setStyle(Paint.Style.STROKE);
        paintFrameOutline.setStrokeWidth(toPx(2));

        paintFrameOutlineSelected.setARGB(255, 66, 134, 244);
        paintFrameOutlineSelected.setAntiAlias(true);
        paintFrameOutlineSelected.setStyle(Paint.Style.STROKE);
        paintFrameOutlineSelected.setStrokeWidth(toPx(2));

        paintText.setColor(Color.WHITE);
        paintText.setTextSize(toPx(15));
        paintText.setTextAlign(Paint.Align.CENTER);
        paintText.setStyle(Paint.Style.FILL);
    }

    protected TileLetter getTileLetter(){
        return this.tileLetter;
    }

    protected void setTileLetter(TileLetter tileLetter){
        this.tileLetter = tileLetter;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        //Draw background
        RectF bg = new RectF(2, 2, this.getWidth()-2, this.getHeight()-2);
        if(this.tileLetter != null && this.tileLetter.isActive())
            canvas.drawRoundRect(bg, toPx(7), toPx(7), this.paintBgActive);
        else
            canvas.drawRoundRect(bg, toPx(7), toPx(7), this.paintBg);

        //Draw outline
        RectF outLine = new RectF(2, 2, this.getWidth()-2, this.getHeight()-2);
        if(this.tileLetter != null && this.tileLetter.isChoosen())
            canvas.drawRoundRect(outLine, toPx(7), toPx(7), this.paintFrameOutlineSelected);
        else
            canvas.drawRoundRect(outLine, toPx(7), toPx(7), this.paintFrameOutline);

        //Draw Text
        if(this.tileLetter != null) {
            int xPos = (this.getWidth() / 2);
            int yPos = (int) ((this.getHeight() / 2) - ((this.paintText.descent() + this.paintText.ascent()) / 2));
            canvas.drawText(this.tileLetter.getLetter(), xPos, yPos, this.paintText);
        }
    }

    /**
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float touchX = event.getX();
        float touchY = event.getY();

        switch (event.getAction()){
            case MotionEvent.ACTION_DOWN:
                this.isActive = !this.isActive;
                break;
        }
        invalidate();
        return true;
    }**/

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int desiredWidth = (int) toPx(25);
        int desiredHeight = (int) toPx(25);

        int widthMode = (int) toPx(MeasureSpec.getMode(widthMeasureSpec));
        int widthSize = (int) toPx(MeasureSpec.getSize(widthMeasureSpec));
        int heightMode = (int) toPx(MeasureSpec.getMode(heightMeasureSpec));
        int heightSize = (int) toPx(MeasureSpec.getSize(heightMeasureSpec));

        int width;
        int height;

        //Measure Width
        if (widthMode == MeasureSpec.EXACTLY) {
            //Must be this size
            width = widthSize;
        } else if (widthMode == MeasureSpec.AT_MOST) {
            //Can't be bigger than...
            width = Math.min(desiredWidth, widthSize);
        } else {
            //Be whatever you want
            width = desiredWidth;
        }

        //Measure Height
        if (heightMode == MeasureSpec.EXACTLY) {
            //Must be this size
            height = heightSize;
        } else if (heightMode == MeasureSpec.AT_MOST) {
            //Can't be bigger than...
            height = Math.min(desiredHeight, heightSize);
        } else {
            //Be whatever you want
            height = desiredHeight;
        }

        setMeasuredDimension(width, height);
        this.invalidate();
    }

    private float toPx(int dp){
        Resources r = getResources();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics());
    }
}
