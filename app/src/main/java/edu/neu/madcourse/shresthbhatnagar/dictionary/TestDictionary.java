package edu.neu.madcourse.shresthbhatnagar.dictionary;

import android.content.DialogInterface;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import edu.neu.madcourse.shresthbhatnagar.hellomad.R;

public class TestDictionary extends AppCompatActivity {
    static final String ET_WORD = "ET_WORD";
    static final String WORD_FOUND = "WORDS_FOUND";

    AsyncTask<BufferedReader, String, Void> wordLoader;

    HashMap<String, Boolean> words = new HashMap<>();
    Trie trie = new Trie();
    EditText et_word;
    ListView lv_worldlist;
    ArrayAdapter adapter;

    ArrayList<String> wordsFound = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_dictionary);

        //Bind variables to views
        et_word = (EditText) this.findViewById(R.id.et_word);
        lv_worldlist = (ListView) findViewById(R.id.lv_words);

        //Load saved instances
        if(savedInstanceState != null){
            et_word.setText(savedInstanceState.getString(ET_WORD));
            wordsFound = savedInstanceState.getStringArrayList(WORD_FOUND);
        }

        //Set ListViewAddapter (Bind ArrayList to ListView)
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, this.wordsFound);
        lv_worldlist.setAdapter(adapter);

        //Load Word Dictionary
        //this.loadWords();

        //Set editText listeners
        et_word.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void afterTextChanged(Editable editable) {
                String word = et_word.getText().toString().toLowerCase();

                if(word.length() == 1)
                    loadWords(word.substring(0, 1));

                if(word.length() < 3)
                    return;

                Log.i("TVVAL", word);
                if(words.containsKey(word)){
                    if(!wordsFound.contains(word)) {
                        wordsFound.add(word);
                        adapter.notifyDataSetChanged();
                    }
                    playBeep();
                }
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(ET_WORD, this.et_word.getText().toString());
        outState.putStringArrayList(WORD_FOUND, this.wordsFound);
    }


    private void loadWords(String first) {
        AssetManager assetManager = this.getAssets();

        InputStream is = null;
        try {
            is = assetManager.open("dict_words/dict." + first + ".txt");
        } catch (Exception ex) {
            ex.printStackTrace();
            return;
        }
        BufferedReader br = new BufferedReader(new InputStreamReader(is));


        this.wordLoader = new WordLoader();
        this.wordLoader.execute(br);
    }

    class WordLoader extends AsyncTask<BufferedReader, String, Void> {

        @Override
        protected Void doInBackground(BufferedReader... reader){
            try {
                String line;
                while((line = reader[0].readLine()) != null) {
                    words.put(line, true);
                }
            }catch(Exception ex){
                Log.d("Error:", ex.getMessage());
            }

            return null;
        }

    }

    @Override
    protected void onStop(){
        super.onStop();
        if(this.wordLoader != null)
            this.wordLoader.cancel(true);
    }

    private void playBeep(){
        MediaPlayer mp = MediaPlayer.create(getApplicationContext(), R.raw.beep);
        mp.start();
        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                mediaPlayer.release();
            }
        });
    }

    public void clear(View view) {
        this.et_word.setText("");
        this.wordsFound.clear();
        adapter.notifyDataSetChanged();
    }

    public void showAcnowledgements(View view) {
        AlertDialog alertDialog = new AlertDialog.Builder(TestDictionary.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("Strategy:\n" +
                "When the first letter of a word is typed, a file containing all the words that start with that letter is loaded into memory. This allows the loading of only whats needed at the time. This is done using an AsyncTask as to avoid UI lag. The file sizes are relatively small so the words are loaded in a few ms\n" +
                "\n" +
                "Resources Used:\n" +
                "All designes are from Android's theme. The Beep sound is a tone I generated.\n" +
                "\n" +
                "External Code Used:\n" +
                "Everything was coded by my self. No code from other sources were used\n" +
                "\n" +
                "Help:\n" +
                "No help was received from anyone");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    public void returnToMenu(View view) {
        finish();
    }
}
