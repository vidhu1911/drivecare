package edu.neu.madcourse.shresthbhatnagar.drivecare.activities;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataItem;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.Wearable;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import edu.neu.madcourse.shresthbhatnagar.hellomad.R;

public class WearSensor extends AppCompatActivity implements DataApi.DataListener, GoogleApiClient.ConnectionCallbacks {
    public static final String TAG = WearSensor.class.getSimpleName();

    //Views
    TextView tvx;
    TextView tvy;
    TextView tvz;

    //Data
    GoogleApiClient googleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dc_activity_wear_sensor);

        initViews();

        this.googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .addConnectionCallbacks(this)
                .build();
    }

    @Override
    protected void onStart() {
        super.onStart();
        this.googleApiClient.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Wearable.DataApi.removeListener(this.googleApiClient, this);
        new Thread(new Runnable() {
            @Override
            public void run() {
                sendMeasurementCommand("stop");
                googleApiClient.disconnect();
            }
        }).start();
    }

    protected void initViews(){
        this.tvx = (TextView) findViewById(R.id.tv_x);
        this.tvy = (TextView) findViewById(R.id.tv_y);
        this.tvz = (TextView) findViewById(R.id.tv_z);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Wearable.DataApi.addListener(this.googleApiClient, this);

        new Thread(new Runnable() {
            @Override
            public void run() {
                sendMeasurementCommand("start");
            }
        }).start();
    }


    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "GAPI Disconnected");
    }


    @Override
    public void onDataChanged(DataEventBuffer dataEventBuffer) {
        for(DataEvent event : dataEventBuffer){
            Log.d(TAG, String.valueOf(event.getType()));
            if(event.getType() == DataEvent.TYPE_CHANGED) {
                DataItem item = event.getDataItem();
                if(item.getUri().getPath().compareTo("/acc") == 0) {
                    DataMap dataMap = DataMapItem.fromDataItem(item).getDataMap();
                    float[] values = dataMap.getFloatArray("values");
                    this.updateSensorReadings(values);
                }
            }
        }
    }

    protected void sendMeasurementCommand(final String cmd){
        Log.d(TAG, "Sending Command");
        List<Node> nodes = Wearable.NodeApi.getConnectedNodes(googleApiClient).await().getNodes();
        for(Node node : nodes) {
            Wearable.MessageApi.sendMessage(this.googleApiClient, node.getId(), "/"+cmd, null)
                    .setResultCallback(new ResultCallback<MessageApi.SendMessageResult>() {
                        @Override
                        public void onResult(@NonNull MessageApi.SendMessageResult sendMessageResult) {
                            Log.d(TAG, "controlMeasurementInBackground(/" + cmd +"): " + sendMessageResult.getStatus().isSuccess());
                        }
                    });
        }
    }

    protected void updateSensorReadings(float[] values){
        this.tvx.setText(String.valueOf(values[0]));
        this.tvy.setText(String.valueOf(values[1]));
        this.tvz.setText(String.valueOf(values[2]));
    }
}
