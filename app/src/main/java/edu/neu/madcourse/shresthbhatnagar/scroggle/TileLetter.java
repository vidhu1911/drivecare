package edu.neu.madcourse.shresthbhatnagar.scroggle;


import android.view.View;

/**
 * Created by vidhu on 2/13/2017.
 */

public class TileLetter {



    private GameFragment mGame;
    private TileLetter mSubTiles[];
    private LetterTileView mView;

    private String letter = "A";
    private boolean isActive = true;
    private boolean isChoosen = false;

    public TileLetter(){}

    public TileLetter(GameFragment game) {
        this.mGame = game;
    }

    public TileLetter[] getSubTiles() {
        return this.mSubTiles;
    }

    public void setSubTiles(TileLetter[] subTiles) {
        this.mSubTiles = subTiles;
    }

    public View getView() {
        return this.mView;
    }

    public void setView(LetterTileView view) {
        this.mView = view;
        this.mView.setTileLetter(this);
    }

    public boolean isChoosen() {
        return isChoosen;
    }

    public void setChoosen(boolean choosen) {
        isChoosen = choosen;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public String getLetter() {
        return letter;
    }

    public void setLetter(String letter) {
        this.letter = letter;
    }

    public void updateDrawableState() {
        this.mView.invalidate();
    }

}
