package edu.neu.madcourse.shresthbhatnagar.scroggle2.game;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Vibrator;

import edu.neu.madcourse.shresthbhatnagar.hellomad.R;
/**
 * Created by vidhu on 3/20/2017.
 */

public class SoundPlayer {
    Vibrator v;
    SoundPool soundPool;
    int clickSound;
    int successSound;

    public SoundPlayer(Context context){
        this.v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        this.soundPool = new SoundPool(10, AudioManager.STREAM_MUSIC, 0);
        this.clickSound = this.soundPool.load(context, R.raw.button_29, 1);
        this.successSound = this.soundPool.load(context, R.raw.success, 1);
    }

    public void playClick() {
        this.v.vibrate(10);
        this.soundPool.play(this.clickSound, 1, 1, 1, 0, 1);
    }

    public void playSuccess() {
        this.v.vibrate(10);
        this.soundPool.play(this.successSound, 1, 1, 1, 0, 1);
    }

}
