package edu.neu.madcourse.shresthbhatnagar.com.test;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;

import edu.neu.madcourse.shresthbhatnagar.hellomad.R;
import edu.neu.madcourse.shresthbhatnagar.com.api.Api;
import edu.neu.madcourse.shresthbhatnagar.com.api.LeaderBoardApi;

public class Leaderboard extends AppCompatActivity {

    ListView lv_leaderboard;
    ArrayList<String> leaderboard;
    ArrayAdapter<String> leaderboard_arrayadapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_activity_leaderboard);

        //Init ref to views
        this.leaderboard = new ArrayList<>();
        this.lv_leaderboard = (ListView) findViewById(R.id.lv_test_leaderboard);

        //Init class vars
        this.leaderboard_arrayadapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, leaderboard);
        this.lv_leaderboard.setAdapter(leaderboard_arrayadapter);

        //Init listeners
        Api.getInstance().getLeaderBoardApi().setLeaderboardItemListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ArrayList<LeaderBoardApi.LeaderBoardEntry> entries = new ArrayList<>();

                for(DataSnapshot ds : dataSnapshot.getChildren()){
                    LeaderBoardApi.LeaderBoardEntry le = ds.getValue(LeaderBoardApi.LeaderBoardEntry.class);
                    entries.add(le);
                }

                Collections.sort(entries);
                Collections.reverse(entries);
                leaderboard.clear();
                for(LeaderBoardApi.LeaderBoardEntry le : entries){
                    leaderboard.add(le.toString());
                }
                leaderboard_arrayadapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("Leaderboard", "opps, error");
            }
        });
    }
}
