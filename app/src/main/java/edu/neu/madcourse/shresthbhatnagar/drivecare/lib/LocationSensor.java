package edu.neu.madcourse.shresthbhatnagar.drivecare.lib;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

/**
 * Created by vidhu on 4/11/2017.
 */

public class LocationSensor implements GoogleApiClient.ConnectionCallbacks {
    private static final String TAG = LocationSensor.class.getSimpleName();

    private Context context;
    private GoogleApiClient googleApiClient;
    private LocationRequest locationRequest;
    private LocationListener locationListener;

    public LocationSensor(Context context, LocationListener locationListener) {
        this.context = context;
        this.locationListener = locationListener;

        this.googleApiClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .build();

        this.locationRequest = new LocationRequest();
        this.locationRequest.setInterval(5000);
        this.locationRequest.setFastestInterval(1000);
        this.locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    public void start() {
        this.googleApiClient.connect();
    }

    public void stop() {
        this.googleApiClient.disconnect();
    }

    private void listenForLocation(){
        if(!checkPermission(this.context))
            return;
        LocationServices.FusedLocationApi.requestLocationUpdates(this.googleApiClient, this.locationRequest, this.locationListener);
    }

    public boolean checkPermission(Context context){
        return ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        this.listenForLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }
}
