package edu.neu.madcourse.shresthbhatnagar.scroggle2.api;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.IgnoreExtraProperties;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

/**
 * Created by vidhu on 3/14/2017.
 */

public class UserApi {

    private static FirebaseDatabase db = FirebaseDatabase.getInstance();
    private static String username;

    /**
     * Sets listener for available users. i.e those who are online
     * @param eventListener
     */
    public static void setAvailableUsersListener(ValueEventListener eventListener) {
        DatabaseReference users = db.getReference("users");
        users.addValueEventListener(eventListener);
    }


    /**
     * Adds user to available list
     * @param username
     */
    public static void addAvailableUser(String username) {
        UserApi.username = username;
        DatabaseReference users = db.getReference("users");
        users.push().setValue(new UserApi.User(username));
    }

    /**
     * Remove user from available list
     */
    public static void removeAvailableUser() {
        DatabaseReference users = db.getReference("users");
        users.orderByChild("username").equalTo(UserApi.username).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    snapshot.getRef().removeValue();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        UserApi.username = null;
    }

    /**
     * Returns the firebase token for the current user
     * @return
     */
    public static String getCurrentUserId() {
        return FirebaseInstanceId.getInstance().getToken();
    }

    public static String getUsername() {
        if(username == null)
            return "Vidhu";
        return username;
    }

    @IgnoreExtraProperties
    public static class User {
        public String id;
        public String username;

        public User(){

        }

        public User(String username, String id){
            this.username = username;
            this.id = id;
        }

        public User(String username){
            this(username, FirebaseInstanceId.getInstance().getToken());
        }

        @Override
        public String toString() {
            return username;
        }
    }
}
