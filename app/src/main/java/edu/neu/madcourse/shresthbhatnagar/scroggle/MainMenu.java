package edu.neu.madcourse.shresthbhatnagar.scroggle;

import edu.neu.madcourse.shresthbhatnagar.hellomad.R;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;


public class MainMenu extends AppCompatActivity {

    Button btnNew;
    Button btnContinue;
    Button btnHelp;
    Button btnAbout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_scroggle_menu);

        this.btnNew = (Button) findViewById(R.id.btn_scr_new);
        this.btnContinue = (Button) findViewById(R.id.btn_scr_continue);
        this.btnHelp = (Button) findViewById(R.id.btn_scr_help);
        this.btnAbout = (Button) findViewById(R.id.btn_scr_about);

        this.btnNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), GameActivity.class);
                startActivity(intent);
            }
        });

        this.btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), GameActivity.class);
                intent.putExtra(GameActivity.KEY_RESTORE, true);
                startActivity(intent);
            }
        });

        this.btnHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), HelpActivity.class);
                startActivity(intent);
            }
        });

        this.btnAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), AcknowledgementActivity.class);
                startActivity(intent);
            }
        });
    }

}
