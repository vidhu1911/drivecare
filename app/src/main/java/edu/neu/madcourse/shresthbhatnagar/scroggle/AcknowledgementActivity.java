package edu.neu.madcourse.shresthbhatnagar.scroggle;

import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.webkit.WebView;
import android.widget.TextView;

import org.w3c.dom.Text;

import edu.neu.madcourse.shresthbhatnagar.hellomad.R;

public class AcknowledgementActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scroggle_acknowledgement);

        WebView tvScrAbout = (WebView) findViewById(R.id.tv_scrAbout);
        tvScrAbout.getSettings().setJavaScriptEnabled(true);
        //ScrAbout.loadData(getString(R.string.scroggleAbout), "text/html; charset=utf-8");
        tvScrAbout.loadData(getString(R.string.scroggleAbout), "text/html", null);


    }
}
