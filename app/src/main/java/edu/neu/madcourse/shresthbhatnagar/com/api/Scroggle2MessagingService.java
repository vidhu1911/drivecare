package edu.neu.madcourse.shresthbhatnagar.com.api;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONObject;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 * Created by vidhu on 2/28/2017.
 */

public class Scroggle2MessagingService extends FirebaseMessagingService {
    private static final String TAG = Scroggle2MessagingService.class.getSimpleName();
    private static final String SERVER_KEY = "key=AAAAqxwTMu8:APA91bEa5pMEM4ET0hjvRtl499r1N8B7gIaW-TYWqh3WXYTXufZiSOoxUWohFPn23NTPmpyTrJetgAbCYKYWotSzViIemUsRePfUboWuS3EJ3Xhq50LB8of2Y9aynwZa2PqsAawtwAtj";

    static List<OnMessageReceiveListener> onMessageReceiveListeners = new ArrayList<>();

    public static void addOnMessageReceiveListener(OnMessageReceiveListener listener) {
        Scroggle2MessagingService.onMessageReceiveListeners.add(listener);
    }

    public static void notifyOnMessageReceiveListeners(Map<String, String> data){
        for(OnMessageReceiveListener listener : onMessageReceiveListeners){
            listener.onMessage(data);
        }
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
            notifyOnMessageReceiveListeners(remoteMessage.getData());
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            //sendNotification(remoteMessage.getNotification().getBody());
        }
    }


    public static void sendMessage(String to, Map<String, Object> data){
        sendMessage(to, data, null);
    }

    public static void sendMessage(String to, final Map<String, Object> data, final Map<String, String> notification){
        final String _to = to;
        final JSONObject _data = data == null ? null : new JSONObject(data);
        final JSONObject _notification = notification == null ? null : new JSONObject(notification);

        new Thread(new Runnable() {
            @Override
            public void run() {
                JSONObject jPayload = new JSONObject();

                try {
                    jPayload.put("to", _to);
                    jPayload.put("priority", "high");

                    if(_notification != null)
                        jPayload.put("notification", _notification);

                    if(_data != null)
                        jPayload.put("data", _data);

                    URL url = new URL("https://fcm.googleapis.com/fcm/send");
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Authorization", SERVER_KEY);
                    conn.setRequestProperty("Content-Type", "application/json");
                    conn.setDoOutput(true);

                    // Send FCM message content.
                    OutputStream outputStream = conn.getOutputStream();
                    outputStream.write(jPayload.toString().getBytes());
                    outputStream.close();

                    // Read FCM response.
                    InputStream inputStream = conn.getInputStream();
                    final String resp = convertStreamToString(inputStream);

                    Handler h = new Handler(Looper.getMainLooper());
                    h.post(new Runnable() {
                        @Override
                        public void run() {
                            Log.e("GAMEApi", "run: " + resp);
                        }
                    });

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }).start();
    }

    private static String convertStreamToString(InputStream is) {
        Scanner s = new Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next().replace(",", ",\n") : "";
    }

    public static interface OnMessageReceiveListener {
        void onMessage(Map<String, String> data);
    }
}
