package edu.neu.madcourse.shresthbhatnagar.drivecare.adapters;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.content.Intent;

import edu.neu.madcourse.shresthbhatnagar.drivecare.activities.TripDetails;
import edu.neu.madcourse.shresthbhatnagar.hellomad.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by vidhu on 4/19/2017.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.TripSummaryHolder> {

    private ArrayList<TripSummary> tripSummaries;

    public RecyclerAdapter(ArrayList<TripSummary> summaries){
        this.tripSummaries = summaries;
    }

    @Override
    public RecyclerAdapter.TripSummaryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflatedView = LayoutInflater.from(parent.getContext()).inflate(R.layout.dc_triphistory_item, parent, false);
        return new TripSummaryHolder(inflatedView);
    }

    @Override
    public void onBindViewHolder(RecyclerAdapter.TripSummaryHolder holder, int position) {
        TripSummary tripSummary = tripSummaries.get(position);
        holder.bind(tripSummary);
    }

    @Override
    public int getItemCount() {
        return this.tripSummaries.size();
    }

    public static class TripSummaryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView iv_map;
        private TextView tv_date;
        private TripSummary tripSummary;

        public TripSummaryHolder(View itemView) {
            super(itemView);

            this.iv_map = (ImageView) itemView.findViewById(R.id.staticMap);
            this.tv_date = (TextView) itemView.findViewById(R.id.tripDate);
            itemView.setOnClickListener(this);
        }

        public void bind(TripSummary tripSummary) {
            this.tripSummary = tripSummary;
            this.iv_map.setImageBitmap(tripSummary.getMap());
            this.tv_date.setText(tripSummary.getDate());
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(v.getContext(), TripDetails.class);
            intent.putExtra("FILENAME", this.tripSummary.getFileName());
            v.getContext().startActivity(intent);
        }
    }

    public static class TripSummary implements Comparable<TripSummary> {

        private Bitmap map;
        private String fileName;

        public TripSummary(Bitmap map, String fileName){
            this.map = map;
            this.fileName = fileName;
        }

        public Bitmap getMap() {
            return map;
        }

        public String getFileName() {
            return fileName;
        }

        public String getDate() {

            try {
                SimpleDateFormat fromFile = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
                Date date = fromFile.parse(this.getDateFromFileName());
                SimpleDateFormat day = new SimpleDateFormat("d");
                SimpleDateFormat month = new SimpleDateFormat("MMMM");
                SimpleDateFormat time = new SimpleDateFormat("h:mm a");
                return day.format(date) + getDayNumberSuffix(Integer.decode(day.format(date))) + " " + month.format(date) + "  " + time.format(date);
            }catch (Exception ex){ ex.printStackTrace(); }
            //return "1st july 1:37 AM";

            return this.getDateFromFileName();
        }

        public Date getDateObject(){
            SimpleDateFormat fromFile = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
            Date date = null;
            try {
                 date = fromFile.parse(this.getDateFromFileName());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return date;
        }

        private String getDateFromFileName(){
            return this.fileName.split("\\.")[1];
        }

        private String getDayNumberSuffix(int day) {
            if (day >= 11 && day <= 13) {
                return "th";
            }
            switch (day % 10) {
                case 1:
                    return "st";
                case 2:
                    return "nd";
                case 3:
                    return "rd";
                default:
                    return "th";
            }
        }

        @Override
        public int compareTo(@NonNull TripSummary o) {
            return -1 * this.getDateObject().compareTo(o.getDateObject());
        }
    }
}
