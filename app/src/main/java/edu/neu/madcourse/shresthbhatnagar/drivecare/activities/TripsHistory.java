package edu.neu.madcourse.shresthbhatnagar.drivecare.activities;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FilenameUtils;

import edu.neu.madcourse.shresthbhatnagar.drivecare.adapters.RecyclerAdapter;
import edu.neu.madcourse.shresthbhatnagar.hellomad.R;

public class TripsHistory extends AppCompatActivity {

    private ArrayList<RecyclerAdapter.TripSummary> tripSummaries = new ArrayList();

    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private RecyclerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dc_activity_trips_history);
        setTitle("Past Trips");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        fillTrips();
        initView();
    }

    protected void initView(){
        this.recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        this.linearLayoutManager = new LinearLayoutManager(this);
        this.recyclerView.setLayoutManager(this.linearLayoutManager);
        this.adapter = new RecyclerAdapter(tripSummaries);
        this.recyclerView.setAdapter(this.adapter);
    }

    private void fillData(){
        Bitmap map = BitmapFactory.decodeResource(getResources(), R.drawable.profile_pic);
        for(int i=0; i<10; i++){
            this.tripSummaries.add(new RecyclerAdapter.TripSummary(map, String.valueOf(i)));
        }
        Collections.sort(this.tripSummaries);
    }

    private void fillTrips() {
        Map<String, File> dataFiles = new HashMap<>();
        Map<String, File> mapFiles = new HashMap<>();
        File path = new File(Environment.getExternalStorageDirectory() + "/drivecare");
        File[] files = path.listFiles();
        if(files == null) return;
        for(File f : files){

            String filename = FilenameUtils.removeExtension(f.getName());
            String ext = FilenameUtils.getExtension(f.getName());

            if(ext.equalsIgnoreCase("json")){
                dataFiles.put(filename, f);
            }else if(ext.equalsIgnoreCase("png")){
                mapFiles.put(filename, f);
            }
        }


        Bitmap bitmap = null;
        RecyclerAdapter.TripSummary summary;
        for(Map.Entry<String, File> data: dataFiles.entrySet()){
            if(mapFiles.containsKey(data.getKey())) {
                try {
                    bitmap = BitmapFactory.decodeFile(mapFiles.get(data.getKey()).getCanonicalPath());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else{
                bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.map_placeholder);
            }
            summary = new RecyclerAdapter.TripSummary(bitmap, data.getKey());
            tripSummaries.add(summary);
        }
        Collections.sort(this.tripSummaries);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                this.onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
