package edu.neu.madcourse.shresthbhatnagar.drivecare.activities;

import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.Messenger;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.content.Intent;
import android.widget.Button;
import android.widget.Toast;

import edu.neu.madcourse.shresthbhatnagar.drivecare.services.DriveMonitorService;
import edu.neu.madcourse.shresthbhatnagar.hellomad.R;

public class StartMonitor extends AppCompatActivity implements View.OnClickListener{

    private Button btn_hideApp;
    private Button btn_endTrip;
    private Button btn_cancelApp;

    private DriveMonitorServiceConnection serviceConnection = new DriveMonitorServiceConnection();
    private DriveMonitorService driveMonitorService;
    boolean bound = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dc_activity_start_monitor);
        setTitle(R.string.dc_appname);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initViews();
    }

    @Override
    protected void onResume() {
        super.onResume();

        boolean start = this.getIntent().getBooleanExtra("START", false);

        Intent intent = new Intent(this, DriveMonitorService.class);
        bindService(intent, serviceConnection, 0);
        if(start) {
            startService(intent);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        //if(this.bound) {
            unbindService(serviceConnection);
            bound = false;
        //}
    }

    private void initViews() {
        this.btn_hideApp = (Button) findViewById(R.id.btn_HideApp);
        this.btn_endTrip = (Button) findViewById(R.id.btn_endTrip);
        this.btn_cancelApp = (Button) findViewById(R.id.btn_cancelTrip);
        this.btn_hideApp.setOnClickListener(this);
        this.btn_endTrip.setOnClickListener(this);
        this.btn_cancelApp.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_HideApp:
                ActivityCompat.finishAffinity(this);
                break;
            case R.id.btn_endTrip:
                if(this.bound){
                    driveMonitorService.stopMonitoring();
                    Intent intent = new Intent(this, DriveCare.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
                break;
            case R.id.btn_cancelTrip:
                if(this.bound){
                    driveMonitorService.cancelMonitoring();
                    Intent intent = new Intent(this, DriveCare.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
                break;
        }
    }

    class DriveMonitorServiceConnection implements ServiceConnection {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            DriveMonitorService.LocalBinder binder = (DriveMonitorService.LocalBinder) service;
            driveMonitorService = binder.getService();
            bound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            bound = false;
        }
    }
}
