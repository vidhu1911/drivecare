package edu.neu.madcourse.shresthbhatnagar.scroggle2.game;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vidhu on 3/14/2017.
 */

public class TiltSensor implements SensorEventListener {
    private List<TiltChangeListener> listeners = new ArrayList<>();
    private SensorManager sensorManager;
    private Sensor sensor;
    private float threshhold;

    int currentTilt = 0;

    public TiltSensor(Context context) {
        this(context, 8.0f);
    }

    public TiltSensor(Context context, float threshhold){
        this.sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        this.sensor = this.sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        this.threshhold = threshhold;
    }

    public void addListener(TiltChangeListener tiltChangeListener) {
        this.listeners.add(tiltChangeListener);
    }

    public void removeListener(TiltChangeListener tiltChangeListener) {
        this.listeners.remove(tiltChangeListener);
    }

    public void stopSensor() {
        sensorManager.unregisterListener(this);
    }

    public void startSensor() {
        sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        int tilt = 0;

        float x = event.values[0];
        if(x < threshhold*-1){
            tilt = -1;
        }else if(x > threshhold){
            tilt = 1;
        }
        if(tilt != this.currentTilt){
            for(TiltChangeListener listener : this.listeners){
                listener.onTiltChange(tilt);
            }
        }
        this.currentTilt = tilt;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public interface TiltChangeListener {
        void onTiltChange(int direction);
    }
}
